package xinghui.Object;

import xinghui.utils.MysqlOnlineObject;
import xinghui.utils.UtilsApi;
import xinghui.utils.UtilsImpl;

public class Account {

    private Integer id;

    private String phone;

    private String password;

    private String name;

    private String base64Img;

    private String Birthday;

    private String sex;

    private String region;

    private Integer goodsCarCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBase64Img() {
        return base64Img;
    }

    public void setBase64Img(String base64img) {
        this.base64Img = base64img;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getGoodsCarCount() {
        return goodsCarCount;
    }

    public void setGoodsCarCount(Integer goodsCarCount) {
        this.goodsCarCount = goodsCarCount;
    }

    public Account() {
    }

    public Account(String phone) {
        UtilsApi utilsApi = new UtilsImpl();
        this.phone = phone;
        String sql = "select * from account where phone = " + phone;
        MysqlOnlineObject query = utilsApi.insertDate(sql, "query");
        try {
            while (query.getResultSet().next()){
                this.id = query.getResultSet().getInt("id");
                this.password = query.getResultSet().getString("password");
                this.name = query.getResultSet().getString("name");
                this.base64Img = query.getResultSet().getString("base64Img");
                this.Birthday = query.getResultSet().getString("Birthday");
                this.sex = query.getResultSet().getString("sex");
                this.region = query.getResultSet().getString("region");
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        sql = "select sum(count) s from goods_car where accountId = "+this.id+";";
        MysqlOnlineObject query1 = utilsApi.insertDate(sql, "query");
        try {
            while (query1.getResultSet().next()){
                this.goodsCarCount = query1.getResultSet().getInt(1);
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
    }


}
