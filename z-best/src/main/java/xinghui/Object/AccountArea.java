package xinghui.Object;

import com.alibaba.fastjson2.JSON;
import xinghui.utils.MysqlOnlineObject;
import xinghui.utils.UtilsApi;
import xinghui.utils.UtilsImpl;

public class AccountArea {
    private Integer id;

    private Integer accountId;

    private String name;

    private Integer phone;

    private String province;

    private String city;

    private String area;

    private String sendYourArea;

    private String territorialIntegrity;

    private Integer postalCode;

    /**
     *
     * @return 返回json类型字符串
     */
    @Override
    public String toString() {
        AccountArea accountArea = new AccountArea(id,accountId,name,phone,province,city,area,sendYourArea,territorialIntegrity,postalCode);
        return JSON.toJSONString(accountArea);
    }

    public AccountArea() {
    }

    public AccountArea(Integer id){
        String sql = "select * from account_area where id = " + id;
        UtilsApi utilsApi = new UtilsImpl();
        MysqlOnlineObject query = utilsApi.insertDate(sql, "query");
        try {
            while (query.getResultSet().next()){
                this.id = id;
                this.accountId = query.getResultSet().getInt(2);
                this.name = query.getResultSet().getString(3);
                this.phone = query.getResultSet().getInt(4);
                this.province = query.getResultSet().getString(5);
                this.city = query.getResultSet().getString(6);
                this.area = query.getResultSet().getString(7);
                this.sendYourArea = query.getResultSet().getString(8);
                this.territorialIntegrity = query.getResultSet().getString(9);
                this.postalCode = query.getResultSet().getInt(10);
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
    }

    public AccountArea(Integer id, Integer accountId, String name, Integer phone, String province, String city, String area, String sendYourArea, String territorialIntegrity, Integer postalCode) {
        this.id = id;
        this.accountId = accountId;
        this.name = name;
        this.phone = phone;
        this.province = province;
        this.city = city;
        this.area = area;
        this.sendYourArea = sendYourArea;
        this.territorialIntegrity = territorialIntegrity;
        this.postalCode = postalCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSendYourArea() {
        return sendYourArea;
    }

    public void setSendYourArea(String sendYourArea) {
        this.sendYourArea = sendYourArea;
    }

    public String getTerritorialIntegrity() {
        return territorialIntegrity;
    }

    public void setTerritorialIntegrity(String territorialIntegrity) {
        this.territorialIntegrity = territorialIntegrity;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }
}
