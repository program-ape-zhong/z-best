package xinghui.Object;

import xinghui.utils.MysqlOnlineObject;
import xinghui.utils.UtilsApi;
import xinghui.utils.UtilsImpl;

import javax.servlet.http.Part;

public class Goods {

    private Integer id;

    private String name;

    private String details;

    private Double price;

    private String[] configurationImg;

    private String[] img;

    private Integer count;

    private String[] configurationText;

    private String type;

    private Integer saleCount;

    public Goods(Integer id){
        String sql = "select * from goods where id = " + id;
        UtilsApi utilsApi = new UtilsImpl();
        MysqlOnlineObject query = utilsApi.insertDate(sql, "query");
        try {
            while (query.getResultSet().next()){
                this.id = id;
                this.name = query.getResultSet().getString("name");
                this.details = query.getResultSet().getString("details");
                this.price = query.getResultSet().getDouble("price");
                this.configurationImg = query.getResultSet().getString("configurationImg").split(",");
                this.img = query.getResultSet().getString("img").split(",");
                this.count = query.getResultSet().getInt("count");
                this.configurationText = query.getResultSet().getString("configurationText").split(",");
                switch (query.getResultSet().getInt("type")){
                    case 1:
                        this.type = "墙式壁挂";
                        break;
                    case 2:
                        this.type = "装饰摆件";
                        break;
                    case 3:
                        this.type = "布衣软式";
                        break;
                    case 4:
                        this.type = "蜡艺香薰";
                        break;
                    case 5:
                        this.type = "创意家居";
                        break;
                }
//                this.type = query.getResultSet().getInt("type");
                this.saleCount = query.getResultSet().getInt("saleCount");
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String[] getConfigurationImg() {
        return configurationImg;
    }

    public void setConfigurationImg(String[] configurationImg) {
        this.configurationImg = configurationImg;
    }

    public String[] getImg() {
        return img;
    }

    public void setImg(String[] img) {
        this.img = img;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String[] getConfigurationText() {
        return configurationText;
    }

    public void setConfigurationText(String[] configurationText) {
        this.configurationText = configurationText;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }


}
