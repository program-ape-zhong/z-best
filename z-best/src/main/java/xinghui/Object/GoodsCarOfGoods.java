package xinghui.Object;

import xinghui.utils.MysqlOnlineObject;
import xinghui.utils.UtilsApi;
import xinghui.utils.UtilsImpl;

public class GoodsCarOfGoods {
    private Integer id;

    private String img;

    private String name;

    private String configurationText;

    private Double price;

    private Integer count;

    private Double sumPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConfigurationText() {
        return configurationText;
    }

    public void setConfigurationText(String configurationText) {
        this.configurationText = configurationText;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(Double sumPrice) {
        this.sumPrice = sumPrice;
    }

    public GoodsCarOfGoods(){}

    public GoodsCarOfGoods(Integer id){
        String sql = "select g.configurationImg,g.`name`,g.configurationText,gc.configurationText,g.price,gc.count," +
                "(g.price * gc.count) sum,gc.id from goods g left join goods_car gc on g.id = gc.goodsId where gc.id = "+ id +";";
        UtilsApi utilsApi =  new UtilsImpl();
        MysqlOnlineObject query = utilsApi.insertDate(sql, "query");
        try {
            while (query.getResultSet().next()){
                String[] split1 = query.getResultSet().getString(1).split(",");
                this.img = split1[query.getResultSet().getInt(4)];
                this.name = query.getResultSet().getString(2);
                String[] split = query.getResultSet().getString(3).split(",");
                this.configurationText = split[query.getResultSet().getInt(4)];
                this.price = query.getResultSet().getDouble(5);
                this.count = query.getResultSet().getInt(6);
                this.sumPrice = query.getResultSet().getDouble(7);
                this.id = query.getResultSet().getInt(8);
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
    }
}
