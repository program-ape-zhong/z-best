package xinghui.dao;
import xinghui.Object.AccountArea;
import xinghui.utils.MysqlOnlineObject;
import xinghui.utils.UtilsApi;
import xinghui.utils.UtilsImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AccountDao {

    private static final UtilsApi UTILS_API = new UtilsImpl();

    //dao执行对数据库的增删改查
    public void haveAccount(String name,String password){

    }

    //注册
    public boolean register(String phone, String password,HttpServletRequest req, HttpServletResponse resp){
        UtilsApi utilsApi = new UtilsImpl();
        //查询是否已经注册
        String sql = "select * from account where phone = " + phone;
        MysqlOnlineObject query = utilsApi.insertDate(sql, "query");
        try {
            while (query.getResultSet().next()){
                req.getSession().setAttribute("hint","Phone已存在，请重新输入");
                return false;
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        //进行注册
        String sql1 = "INSERT INTO account(phone,`password`) VALUES("+ phone +","+ password +")";
        utilsApi.insertDate(sql1,"into");
        req.getSession().setAttribute("hint","注册成功");
        return true;
    }

    public boolean isPhone(String phone) {
        UtilsApi utilsApi = new UtilsImpl();

        String sql = "select * from account where phone = '" + phone + "'";
        MysqlOnlineObject query = utilsApi.insertDate(sql, "query");
        try {
            while (query.getResultSet().next()){
                return true;
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }

        return false;
    }

    public boolean login(String phone, String password) {
        String sql = "select * from account where phone = '" + phone + "' and " + "password = '" + password+"'";
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        try {
            while (query.getResultSet().next()){
                return true;
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        return false;
    }

    public boolean updateInformation(String name, String birthday, String sex, String phone) {
        String sql = "update account set name = '"+name+"'" + ",birthday = '"+birthday+"',sex = '"+sex+"' where phone = '"+phone+"'";
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
        return true;
    }

    public void updateRegion(String region,String name) {
        String sql = "update account set region = '"+ region +"' where `name` = '"+name+"'";
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
    }

    public void updateImg(String base64ByPart, String phone) {
        String sql = "update account set base64Img = '"+base64ByPart+"' where phone = "+phone;
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
    }

    public void updatePassword(String phone, String newPassword) {
        String sql = "update account set password = '"+newPassword+"' where phone = '"+phone+"'";
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
    }

    public void addAccountArea(AccountArea accountArea) {
        String sql = "call add_account_area("+accountArea.getAccountId()+"," +
                "\""+accountArea.getName()+"\","+accountArea.getPhone()+"," +
                "\""+accountArea.getProvince()+"\"," +
                "\""+accountArea.getCity()+"\"," +
                "\""+accountArea.getArea()+"\"," +
                "\""+accountArea.getSendYourArea()+"\"," +
                "\""+accountArea.getTerritorialIntegrity()+"\"," +
                ""+accountArea.getPostalCode()+");";
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
    }

    public List<AccountArea> getAllAccountArea(Integer id) {
        String sql = "select id from account_area where account_id = " + id + "; ";
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        List<AccountArea> list = new ArrayList<>();
        try {
            while (query.getResultSet().next()){
                AccountArea accountArea = new AccountArea(query.getResultSet().getInt(1));
                list.add(accountArea);
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        return list;
    }

//    public String getAllPhone() {
//        UtilsApi utilsApi = new UtilsImpl();
//
//        String sql = "select phone from account";
//        MysqlOnlineObject query = utilsApi.insertDate(sql, "query");
//        String allPhone = "";
//        try {
//            while (query.getResultSet().next()){
//                int phone = query.getResultSet().getInt("phone");
//                allPhone = phone + ",";
//            }
//        }catch (Exception e){
//            System.out.println(e);
//        }
//        return allPhone;
//    }
}
