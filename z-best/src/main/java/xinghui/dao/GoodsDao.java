package xinghui.dao;

import xinghui.Object.GoodsCarOfGoods;
import xinghui.utils.MysqlOnlineObject;
import xinghui.utils.UtilsApi;
import xinghui.utils.UtilsImpl;

import java.util.ArrayList;
import java.util.List;

public class GoodsDao {
    private static final UtilsApi UTILS_API = new UtilsImpl();

    /**
     * 添加商品
     * @param name 名称
     * @param details 详情
     * @param price 价格
     * @param newConfigurationImgArr 购买配置图片 base64或绝对路径
     * @param configurationText 购买配置详情
     * @param img 商品图片
     * @param count 商品数量
     * @param type 商品类型
     */
    public void addGoods(String name, String details, Double price, String[] newConfigurationImgArr, String[] configurationText, String img, int count, String type) {
        String newConfigurationImgArr2 = UTILS_API.splitStringArr(newConfigurationImgArr);
        String configurationText2 = UTILS_API.splitStringArr(configurationText);
        String sql = "insert into goods(name,details,price,configurationImg,img,count,configurationText,type) values" +
                "('"+name+"','"+details+"',"+price+",'"+newConfigurationImgArr2+"','"+img+"',"+count+",'"+configurationText2+"',"+type+")";
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
        System.out.println(sql);
    }

    public void addGoodsCar(int accountId, String goodsId, String configurationText, int count) {
        String sql = "select * from goods_car where accountId = "+ accountId +" and goodsId = '"+ goodsId +"' and configurationText = '"+ configurationText +"' ;";
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        boolean noting = false;
        try {
            while (query.getResultSet().next()){
                noting = true;
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        if (noting){
            sql = "update goods_car set count = count + " + count + " where accountId = '"+ accountId +"' and goodsId = '"+ goodsId +"' and configurationText = '" + configurationText+"';";
        }else {
            sql = "insert into goods_car(accountId,goodsId,configurationText,count) values("+accountId+",'"+goodsId+"','"+configurationText+"',"+count+");";
        }
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
    }

    public List<GoodsCarOfGoods> getGoodsCarOfAllGoods(Integer id) {
        String sql = "select id from goods_car where accountId = " + id;
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        List<GoodsCarOfGoods> list = new ArrayList<>();
        boolean noting = true;
        try {
            while (query.getResultSet().next()){
                GoodsCarOfGoods g = new GoodsCarOfGoods(query.getResultSet().getInt(1));
                list.add(g);
                noting = false;
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        if (noting){
            list.add(new GoodsCarOfGoods());
        }
        return list;
    }

    public void updateGoodsCarCount(String doing2, Integer id) {
        String doing = "add".equals(doing2)?"+":"-";
        String sql = "update goods_car set count = count "+ doing +" 1 where id = " + id;
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
    }

    public void deleteGoodsCarOfGoods(int id) {
        String sql = "delete from goods_car where id  = " + id + ";";
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();
    }

    public void addOrder(Integer id, Double sum) {
        String sql  = "insert into `order`(accountId,date,`status`,delete_flag,money) values("+id+",NOW(),'待支付',0,"+sum+")";
        MysqlOnlineObject into = UTILS_API.insertDate(sql, "into");
        into.close();

    }

    public Long payGoods(String[] ids) {
        for (int i = 0; i < ids.length; i++) {
            String sql = "insert into pay_goods values((select max(id) from `order`),"+ids[i]+")";
            UTILS_API.insertDate(sql,"into");
        }
        String sql = "select max(orderId) from pay_goods";
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        Long id = 0L;
        try {
            while (query.getResultSet().next()){
                id = Long.parseLong(query.getResultSet().getString(1));
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        return id;
    }

    public List<GoodsCarOfGoods> getGoodsCarOfGoodsByOrderId(Long id) {
        String sql = "select goodsCarID from pay_goods where orderId = "+id+"";
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        List<GoodsCarOfGoods> goodsCarOfGoodsList = new ArrayList<>();
        try {
            while (query.getResultSet().next()){
                GoodsCarOfGoods goods = new GoodsCarOfGoods(query.getResultSet().getInt(1));
                goodsCarOfGoodsList.add(goods);
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        return goodsCarOfGoodsList;
    }

    public Integer getGoodsCarIdByGoodsId(Integer id, Integer id1, Integer configurationText) {
        String sql = "select id from goods_car where accountId = " + id + " and goodsId = " + id1 + " and configurationText = " + configurationText + "";
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        Integer goodsCarId = null;
        try {
            while (query.getResultSet().next()){
                goodsCarId = query.getResultSet().getInt(1);
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        return goodsCarId;
    }
}
