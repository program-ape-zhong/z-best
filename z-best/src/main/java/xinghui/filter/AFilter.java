package xinghui.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebFilter("/*")
public class AFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //项目路径
        filterConfig.getServletContext().setAttribute( "ctp",filterConfig.getServletContext() .getContextPath());
        //Account控制器的路径
        filterConfig.getServletContext().setAttribute( "accountPath",filterConfig.getServletContext() .getContextPath()+"/isLogin/AccountController");
        filterConfig.getServletContext().setAttribute( "goodsPath",filterConfig.getServletContext() .getContextPath()+"/isLogin/GoodsController");
        System.out.println("A过滤器初始化"); //项目启动时调用
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//      只有servlet和jsp需要设置字符编码
        //css、js设置字符编码会无法加载
        // 获得用户请求的URI
        HttpServletRequest req = (HttpServletRequest)servletRequest;
        HttpServletResponse resp = (HttpServletResponse)servletResponse;
        String path = req.getRequestURI();
        if (path.contains(".js")){
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }
        // 设置请求信息的解码格式：
        servletRequest.setCharacterEncoding("UTF-8");
        // 设置响应信息的编码格式：
        servletResponse.setCharacterEncoding("UTF-8");
        if (req.getSession().getAttribute("hint")==null){
            req.getSession().setAttribute("hint","");
        }
        //TODO 提示框 能实现但不推荐
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        System.out.println("A过滤器销毁");  //项目关闭时调用
    }
}
