package xinghui.filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebFilter("/isLogin/*")
public class BFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse  response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();

        String doing = request.getParameter("doing");
        //判断是否正在登录        //判断是否正在注册
        if ("register".equals(doing)||"login".equals(doing)||"getCode".equals(doing)||"findPassWord".equals(doing)
                ||"sendSMS".equals(doing)||"updatePassWordByPhone".equals(doing)||"getLoginAccount".equals(doing)){
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }
        //判断是否登录
        if (request.getSession().getAttribute("account")==null){
            request.getSession().setAttribute("hint","请先登录");
            response.sendRedirect(request.getContextPath()+"/login.jsp");
            return;
        }
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
