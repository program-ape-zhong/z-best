package xinghui.service;

import cn.dsna.util.images.ValidateCode;
import com.alibaba.fastjson2.JSON;
import xinghui.Object.Account;
import xinghui.Object.AccountArea;
import xinghui.dao.AccountDao;
import xinghui.utils.UtilsApi;
import xinghui.utils.UtilsImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

@MultipartConfig
public class AccountService {
    //dao负责对数据库的增删改查，将数据返回给service处理
    private AccountDao accountDao = new AccountDao();
    private static final UtilsApi UTILS_API = new UtilsImpl();

    public boolean login(HttpServletRequest req, HttpServletResponse resp){
        String phone = req.getParameter("phone");
        String password = req.getParameter("password");
        if (UTILS_API.isEmpty(phone)|| UTILS_API.isEmpty(password)){
            req.getSession().setAttribute("hint","密码不能为空");
            return false;
        }
        boolean login = accountDao.login(phone, password);

        if (login){
            Account account = new Account(phone);
            req.getSession().setAttribute("account",account);
            GoodsService goodsService = new GoodsService();
            goodsService.getGoodsCarOfAllGoods(req, resp);
            req.getSession().setAttribute("hint","登录成功，欢迎进入商城首页");
        }else {
            req.getSession().setAttribute("hint","账户或密码错误");
        }
        //处理登录的业务逻辑
        return login;
    }

    public boolean register(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //处理注册的业务逻辑
        String phone = req.getParameter("phone");
        String password1 = req.getParameter("password1");
        String password2 = req.getParameter("password2");
        String inputCode = req.getParameter("code");
        String sessionCode = (String) req.getSession().getAttribute("code");
        //判断验证码
        if (!sessionCode.equalsIgnoreCase(inputCode)){
            req.getSession().setAttribute("hint","验证码错误，请重新输入");
            return false;
        }
        if (phone.length()<5){
            req.getSession().setAttribute("hint","手机号长度建议至少六位数");
            return false;
        }
        //判断密码
        if (!Objects.equals(password1,password2)){
            req.getSession().setAttribute("hint","密码不一致，注册失败");
            return false;
        }
        if (password1.length()<5){
            req.getSession().setAttribute("hint","密码长度建议至少六位数");
            return false;
        }
        boolean register = accountDao.register(phone, password1, req, resp);
        return register;

    }

    public void getCode(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UTILS_API.getCode(req, resp);
    }

    public int findPassWord(HttpServletRequest req, HttpServletResponse resp) {
        String phone = req.getParameter("phone");
        String inputCode = req.getParameter("inputCode");
        String sessionCode = (String) req.getSession().getAttribute("code");
        Integer page = Integer.parseInt(req.getParameter("page"));
        //判断用户名、验证码是否正确
        System.out.println("inputCode:"+inputCode+"sessionCode:"+sessionCode);
        if (page==1){
            if (!sessionCode.equalsIgnoreCase(inputCode)){
                req.getSession().setAttribute("hint","验证码错误");
                return 1;
            }
            boolean phone1 = accountDao.isPhone(phone);
            if (phone1){
                return 2;
            }else {
                return 1;
            }
        }
        return 0;
    }

    public void sendAllPhoneAndCode(HttpServletRequest req, HttpServletResponse resp){
        String inputPhone = req.getParameter("inputPhone");
        String inputCode = req.getParameter("inputCode");
        System.out.println("inputPhone="+inputPhone);
        System.out.println("inputCode="+inputCode);
        String code = (String) req.getSession().getAttribute("code");
        boolean phone = accountDao.isPhone(inputPhone);
        resp.setHeader("Access-Control-Allow-Origin", "*"); //解决跨域问题，否则前端无法获取数据
        resp.setContentType("application/json;charset=utf-8");
        if (code.equalsIgnoreCase(inputCode)&&phone){
            try {
                resp.getWriter().print("true");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            //提示错误
            try {
                resp.getWriter().print("false");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void updateInformation(HttpServletRequest req, HttpServletResponse resp) {
        String name = req.getParameter("name");
        String birthday = req.getParameter("birthday");
        if (!UTILS_API.isValidFormat("yyyy-mm-dd", birthday)) {
            req.getSession().setAttribute("hint", "日期格式错误，请重新输入");
            return;
        }
        String sex = req.getParameter("sex");
        Account account = (Account)req.getSession().getAttribute("account");
        String phone = account.getPhone();
        boolean b = accountDao.updateInformation(name, birthday, sex, phone);
        if (b) {
            req.getSession().setAttribute("hint", "更新成功");
            updateAccountSession(req, resp);
        } else {
            req.getSession().setAttribute("hint", "更新失败,请联系管理员处理");
        }
    }

    public void updateRegion(HttpServletRequest req, HttpServletResponse resp) {
        String region = req.getParameter("region");
        Account account = ((Account)req.getSession().getAttribute("account"));
        accountDao.updateRegion(region,account.getName());
        req.getSession().setAttribute("hint","更新成功");
        updateAccountSession(req, resp);
    }

    public void updateAccountSession(HttpServletRequest req, HttpServletResponse resp){
        Account account = (Account)req.getSession().getAttribute("account");
        Account newAccount = new Account(account.getPhone());
        req.getSession().setAttribute("account",newAccount);
    }

    public void updateImg(HttpServletRequest req, HttpServletResponse resp) {
        Part part = null;
        try {
            part = req.getPart("img");
            if (part.getSize()==0){
                req.getSession().setAttribute("hint","请不要执行不必要的操作(╰ (‵□′)╯)");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        String base64ByPart = UTILS_API.getBase64ByPart(part);
        Account account = (Account)req.getSession().getAttribute("account");
        accountDao.updateImg(base64ByPart,account.getPhone());
        req.getSession().setAttribute("hint","更新成功");
        updateAccountSession(req, resp);

    }

    public void updatePassword(HttpServletRequest req, HttpServletResponse resp) {
        String oldPassword = req.getParameter("oldPassword");
        String newPassword = req.getParameter("newPassword");
        String newPassword2 = req.getParameter("newPassword2");
        String code = req.getParameter("code");
        Account account = (Account) req.getSession().getAttribute("account");
        String sessionCode = (String)req.getSession().getAttribute("code");
        if (!account.getPassword().equals(oldPassword)){
            req.getSession().setAttribute("hint","与旧密码不同，无法修改");
            return;
        }
        if (newPassword.length()<5){
            req.getSession().setAttribute("hint","密码长度建议至少六位数，无法修改");
            return;
        }
        if (!newPassword.equals(newPassword2)){
            req.getSession().setAttribute("hint","两次输入的新密码各不相同，无法修改");
            return;
        }
        if (!sessionCode.equalsIgnoreCase(code)){
            req.getSession().setAttribute("hint","验证码错误，无法修改");
            return;
        }
        accountDao.updatePassword(account.getPhone(),newPassword);
        req.getSession().setAttribute("hint","密码修改成功");
        updateAccountSession(req, resp);
    }

    public void updatePassWordByPhone(HttpServletRequest req, HttpServletResponse resp) {
        String noting = "true";
        String inputPhone = req.getParameter("inputPhone");
        String newPassWord = req.getParameter("newPassWord");
        if (newPassWord.length()<5){
            noting = "密码长度建议至少六位数，请重新输入";

        }else {
            accountDao.updatePassword(inputPhone, newPassWord);
            req.getSession().setAttribute("hint","恭喜！！您的密码修改成功");
        }
        try {
            resp.getWriter().write(noting);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addAccountArea(HttpServletRequest req, HttpServletResponse resp) {
        String data = req.getParameter("data");
        AccountArea accountArea = JSON.parseObject(data, AccountArea.class);
        accountDao.addAccountArea(accountArea);
    }

    public void getAllAccountArea(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Account account = (Account)req.getSession().getAttribute("account");
        List<AccountArea> list = accountDao.getAllAccountArea(account.getId());
        resp.getWriter().write(JSON.toJSONString(list));
    }
}
