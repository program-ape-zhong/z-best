package xinghui.service;

import xinghui.Object.Account;
import xinghui.Object.Goods;
import xinghui.Object.GoodsCarOfGoods;
import xinghui.dao.GoodsDao;
import xinghui.utils.MysqlOnlineObject;
import xinghui.utils.UtilsApi;
import xinghui.utils.UtilsImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

public class GoodsService {
    private GoodsDao goodsDao = new GoodsDao();

    private static final UtilsApi UTILS_API = new UtilsImpl();

    public void addGoods(HttpServletRequest req, HttpServletResponse resp) {
        String name = req.getParameter("name"); //名称
        String details = req.getParameter("details"); //详情
        Double price = Double.parseDouble(req.getParameter("price")); //价格
        String[] configurationImg2 = !req.getParameter("configurationImg2").equals("") ?req.getParameter("configurationImg2").split(",") :null; //配置图片路径
        Collection<Part> parts = new ArrayList<>();
        List<Part> configurationImgList = new ArrayList<>(); //配置图片base64
        String img = req.getParameter("img"); //商品图片
        String type = req.getParameter("type");
        try {
            parts = req.getParts();
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
        //将配置图片和图片区分开来 configurationImgList/imgList
        for (Part p:parts) {
            if ("configurationImg".equals(p.getName())&&!"".equals(p.getSubmittedFileName())){
                configurationImgList.add(p);
                System.out.println("configurationImg的Name"+p.getName());
            }
        }
        //判断商品配置信息是否为空
        String[] configurationText = !req.getParameter("configurationText").equals("") ?req.getParameter("configurationText").split(",") :null; //配置详情
        if (configurationText==null){
            req.getSession().setAttribute("hint","配置详情不能为空，请重试");
            return;
        }
        int count = Integer.parseInt(req.getParameter("count")); //库存
        //判断是否上传图片
        if (img.length()==0){
            req.getSession().setAttribute("hint","未上传图片，请重试");
            return;
        }
//        判断配置图片路径和base64是不是都为空
        //如果配置图片路径和base64都为空
        if (configurationImgList.size()==0&&configurationImg2==null){
            req.getSession().setAttribute("hint","配置图片和base不可都为为空");
            return;
        //如果两个都不为空
        }else if (configurationImgList.size()!=0&&configurationImg2!=null){
            req.getSession().setAttribute("hint","配置图片路径和上传不可同时输入");
            return;
        //如果base64不为空
        }else if (configurationImgList.size()!=0){
            //判断配置详情、图片
            if (configurationImgList.size()!=configurationText.length) {
                req.getSession().setAttribute("hint", "配置图片base和配置详情长度不一致");
                return;
            }
            String[] newConfigurationImgArr = new String[configurationImgList.size()];
            for (int i = 0; i < configurationImgList.size(); i++) {
                newConfigurationImgArr[i] = UTILS_API.getBase64ByPart(configurationImgList.get(i));
            }
//            List<String> newConfigurationImgList = new ArrayList<>();
//            for (Part p:configurationImgList) {
//                newConfigurationImgList.add(UTILS_API.getBase64ByPart(p));
//            }
            goodsDao.addGoods(name,details,price,newConfigurationImgArr,configurationText,img,count,type);
        //如果路径不为空
        }else if (configurationImg2!=null){
            //判断配置详情、图片
            if (configurationImg2.length!=configurationText.length){
                req.getSession().setAttribute("hint","配置图片url和配置详情长度不一致");
                return;
            }
            goodsDao.addGoods(name,details,price,configurationImg2,configurationText,img,count,type);
        }
        req.getSession().setAttribute("hint","商品添加成功");
    }

    public void getGoodsListByType(HttpServletRequest req, HttpServletResponse resp) {
        String type = req.getParameter("type");
        List<Goods> list = new ArrayList<>();
        String sql = "select id from goods where type = " + type;
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        try {
            while (query.getResultSet().next()){
                list.add(new Goods(query.getResultSet().getInt("id")));
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        switch (type){
            case "1":
                type = "one";
                break;
            case "2":
                type = "two";
                break;
            case "3":
                type = "three";
                break;
            case "4":
                type = "four";
                break;
            case "5":
                type = "five";
                break;
            default:
                type = "zero";
        }
        req.getSession().setAttribute(type,list);
    }

    public void getGoods(HttpServletRequest req, HttpServletResponse resp) {
        Integer id = Integer.parseInt(req.getParameter("id"));
        req.getSession().setAttribute("goods",new Goods(id));
        getGoodsCarOfAllGoods(req, resp);
    }

    public void getRecommendGoodsList(HttpServletRequest req, HttpServletResponse resp) {
        String sql = "select id from goods limit 0,4";
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        List<Goods> list = new ArrayList<>();
        try {
            while (query.getResultSet().next()){
                list.add(new Goods(query.getResultSet().getInt("id")));
            }
        }catch (Exception e){
            System.out.println(e);
        }finally {
            query.close();
        }
        req.getSession().setAttribute("RecommendGoodsList",list);
    }

    public void getLoveGoodsList(HttpServletRequest req, HttpServletResponse resp) {
        String type = req.getParameter("type");
        switch (type){
            case "墙式壁挂":
                type = "1";
                break;
            case "装饰摆件":
                type = "2";
                break;
            case "布衣软式":
                type = "3";
                break;
            case "蜡艺香薰":
                type = "4";
                break;
            case "创意家居":
                type = "5";
                break;
            default:
                type = "0";
        }
        String sql = "select id from goods where type = "+type;
        MysqlOnlineObject query = UTILS_API.insertDate(sql, "query");
        List<Goods> list = new ArrayList<>();
        try {
            while (query.getResultSet().next()){
                list.add(new Goods(query.getResultSet().getInt("id")));
            }
        }catch (Exception e){
            System.out.println(e);
        }
        req.getSession().setAttribute("LoveGoodsList",list);
    }

    public void addGoodsCar(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println("==================addGoodsCar===============");
        Account account = (Account)req.getSession().getAttribute("account");
        int accountId = account.getId();
        String goodsId = req.getParameter("goodsId");
        String configurationText = req.getParameter("configurationText");
        int count = Integer.parseInt(req.getParameter("count"));
        goodsDao.addGoodsCar(accountId, goodsId, configurationText, count);
        req.getSession().setAttribute("account",new Account(account.getPhone()+""));
        try {
            resp.getWriter().write("ture");
        } catch (IOException e) {
            e.printStackTrace();
        }
        getGoodsCarOfAllGoods(req, resp);
    }

    public void getGoodsCarOfAllGoods(HttpServletRequest req, HttpServletResponse resp) {
        Account account = (Account)req.getSession().getAttribute("account");
        List<GoodsCarOfGoods> goodsCarOfAllGoods = goodsDao.getGoodsCarOfAllGoods(account.getId());
        req.getSession().setAttribute("goodsCarOfAllGoods",goodsCarOfAllGoods);
    }

    public void updateGoodsCarCount(HttpServletRequest req, HttpServletResponse resp) {
        String doing2 = req.getParameter("doing2");
        Integer id = Integer.parseInt(req.getParameter("id"));
        goodsDao.updateGoodsCarCount(doing2,id);
        getGoodsCarOfAllGoods(req, resp);
    }

    public void deleteGoodsCarOfGoods(HttpServletRequest req, HttpServletResponse resp) {
        int id = Integer.parseInt(req.getParameter("id"));
        goodsDao.deleteGoodsCarOfGoods(id);
        getGoodsCarOfAllGoods(req, resp);
    }

    public void addOrder(HttpServletRequest req, HttpServletRequest resp) {
        System.out.println("==================addOrder===============");
        String[] ids = req.getParameter("id").split(",");
        Integer ids2 = null;
        //proDetail页面添加单个商品，直接跳转结算页面
        if ("true".equals(req.getParameter("noting"))){
            Account account = (Account)req.getSession().getAttribute("account");
            Integer id = Integer.valueOf(req.getParameter("id"));
            Integer configurationText = Integer.valueOf(req.getParameter("configurationText"));
            ids = null;
            ids2 = goodsDao.getGoodsCarIdByGoodsId(account.getId(),id,configurationText);
        }

        Account account = (Account) req.getSession().getAttribute("account");
        Double sum = 0.0;
        if (ids!=null){
            for (int i = 0; i < ids.length; i++) {
                GoodsCarOfGoods goodsCarOfGoods = new GoodsCarOfGoods(Integer.parseInt(ids[i]));
                sum += goodsCarOfGoods.getSumPrice();
            }
        }else {
            GoodsCarOfGoods goodsCarOfGoods = new GoodsCarOfGoods(ids2);
            sum += goodsCarOfGoods.getSumPrice();
        }
        //新增订单
        goodsDao.addOrder(account.getId(),sum);
        //新增pay_goods
        Long id = goodsDao.payGoods(ids==null?(ids2+"").split(","):ids);
        req.getSession().setAttribute("orderId",id);
        //通过id获取订单中的商品
        List<GoodsCarOfGoods> goodsCarOfGoodsByOrderId = goodsDao.getGoodsCarOfGoodsByOrderId(id);
        req.getSession().setAttribute("goodsCarOfGoodsByOrderId",goodsCarOfGoodsByOrderId);
    }
}
