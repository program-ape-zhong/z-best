package xinghui.servlet;


import xinghui.service.AccountService;
import xinghui.utils.UtilsImpl;
import xinghui.utils.sendSMS;
//import xinghui.utils.se

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@MultipartConfig(maxFileSize = 12 * 1024 * 1024 )
@WebServlet("/isLogin/AccountController")
public class AccountController extends HttpServlet {
    //处理业务逻辑
    private AccountService accountService = new AccountService();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //控制器接收请求，具体的业务调用service中方法,根据service方法的返回结果判断是否跳转
        String doing = req.getParameter("doing");
        System.out.println("doing："+ doing);
        switch (doing){
            case "login":
                boolean login = accountService.login(req, resp);
                if (login){
                    resp.sendRedirect(req.getContextPath()+"/isLogin/index.jsp");
                }else {
                    resp.sendRedirect(req.getContextPath()+"/login.jsp");
                }
                break;
            case "register":
                boolean register = accountService.register(req, resp);
                if (register){
                    resp.sendRedirect(req.getContextPath()+"/login.jsp");
                }else {
                    resp.sendRedirect(req.getContextPath()+"/reg.jsp");
                }
                break;
            case "getCode":
                accountService.getCode(req, resp);
                break;
            case "findPassWord": //ajax控制发送和接收
                accountService.sendAllPhoneAndCode(req, resp);
                break;
            case "sendSMS":
                //TODO 找不到类
                try {
                    //可以通过maven中的依赖调用阿里云的api了 具体在->Project Structure->Artifacts中设置
                    String sms = sendSMS.findSMS();
                    resp.setHeader("Access-Control-Allow-Origin", "*"); //解决跨域问题，否则前端无法获取数据
                    resp.setContentType("application/json;charset=utf-8");
                    resp.getWriter().write(sms);
                } catch (Exception e) {
                    System.out.println("短信发送失败");
                    e.printStackTrace();
                }
                break;
            case "updateInformation":
                accountService.updateInformation(req, resp);
                resp.sendRedirect(req.getContextPath()+"/isLogin/mygrxx.jsp");
                break;
            case "updateRegion":
                accountService.updateRegion(req,resp);
                resp.sendRedirect(req.getContextPath()+"/isLogin/mygrxx.jsp");
                break;
            case "updateImg":
                accountService.updateImg(req,resp);
                resp.sendRedirect(req.getContextPath()+"/isLogin/mygrxx.jsp");
                break;
            case "updatePassword":
                accountService.updatePassword(req,resp);
                resp.sendRedirect(req.getContextPath()+"/isLogin/remima.jsp");
                break;
            case "updatePassWordByPhone":
                accountService.updatePassWordByPhone(req, resp);
                break;
            case "addAccountArea":
                accountService.addAccountArea(req,resp);
                break;
            case "getAllAccountArea":
                accountService.getAllAccountArea(req,resp);
                break;
            default:
//                //TODO 跳转到错误页面
        }
    }
}
