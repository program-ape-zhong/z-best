package xinghui.servlet;

import xinghui.service.AccountService;
import xinghui.service.GoodsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/isLogin/GoodsController")
@MultipartConfig(maxFileSize = 50 * 1024 * 1024 )
public class GoodsController extends HttpServlet {
    private GoodsService goodsService = new GoodsService();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String doing = req.getParameter("doing");
        switch (doing){
            case "addGoods":
                goodsService.addGoods(req,resp);
                resp.sendRedirect(req.getContextPath()+"/isLogin/mygrxx.jsp");
            case "getGoodsListByType":
                goodsService.getGoodsListByType(req,resp);
                resp.sendRedirect(req.getContextPath()+"/isLogin/index.jsp");
                break;
            case "getGoods":
                goodsService.getGoods(req,resp);
                break;
            case "getRecommendGoodsList":
                goodsService.getRecommendGoodsList(req,resp);
                break;
            case "getLoveGoodsList":
                goodsService.getLoveGoodsList(req,resp);
                break;
            case "addGoodsCar":
                goodsService.addGoodsCar(req,resp);
                break;
            case "getGoodsCarOfAllGoods":
                goodsService.getGoodsCarOfAllGoods(req,resp);
                break;
            case "updateGoodsCarCount":
                goodsService.updateGoodsCarCount(req,resp);
                break;
            case "deleteGoodsCarOfGoods":
                goodsService.deleteGoodsCarOfGoods(req,resp);
                break;
            case "addOrder":
                goodsService.addOrder(req,req);
                break;
            default:
                //TODO 跳转错误页面
        }

    }
}
