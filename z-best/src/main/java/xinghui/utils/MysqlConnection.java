package xinghui.utils;

import java.sql.*;

public class MysqlConnection {
	// 连接数据库，需要数据库的地址，用户名，密码
	// jdbc:mysql://数据库主机地址:端口号/数据库名称?serverTimezone=GMT%2B8&useSSL=false
	public static final String URL = "jdbc:mysql://127.0.0.1:3306/z-best?serverTimezone=GMT%2B8&useSSL=false";
	public static final String USERNAME = "root";
	public static final String PASSWORD = "123456";
	public static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	
	// 静态代码块，注册驱动
	static {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
    /**
     * 获取数据库连接
     * @return 数据库连接对象
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException throwables) {
            System.out.println("程序出现异常");
            throwables.printStackTrace();
        }
        return conn;
    }
    
    /**
     * 释放资源
     *
     * @param rs
     * @param st
     * @param conn
     */
    public static void close(ResultSet rs, Statement st, Connection conn) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}