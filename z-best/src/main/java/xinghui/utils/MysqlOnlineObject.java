package xinghui.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MysqlOnlineObject {
	
Connection connection;

Statement statement;

ResultSet resultSet;

public Connection getConnection() {
	return connection;
}

public void setConnection(Connection connection) {
	this.connection = connection;
}

public Statement getStatement() {
	return statement;
}

public void setStatement(Statement statement) {
	this.statement = statement;
}

public ResultSet getResultSet() {
	return resultSet;
}

public void setResultSet(ResultSet resultSet) {
	this.resultSet = resultSet;
}

public MysqlOnlineObject(Connection connection, Statement statement, ResultSet resultSet){
	this.connection = connection;
	this.statement = statement;
	this.resultSet = resultSet;
}

//关闭连接
public void close(){
	try {
		connection.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}

}
