package xinghui.utils;


import javax.servlet.http.*;
import java.io.IOException;


public interface UtilsApi {
	//执行sql语句
    MysqlOnlineObject insertDate(String sql, String doing);
    //获取ip
    String getClientIP(HttpServletRequest request);
    //通过cookie名称删除或查询cookie
    String doingCookieByName(HttpServletRequest request, HttpServletResponse response, String cookieName,String doing);
    //通过session创建cookie
    Cookie createCode(HttpServletRequest req, HttpServletResponse resp, HttpSession session,String attribute);
    //密码加密
    String setPassword(String phones);
    //获取图片base64编码格式
    String getBase64ByPart(Part part);
    //判断字符串是否为空
    boolean isEmpty(String str);
    //判断日期格式是否正确
    boolean isValidFormat(String format, String value);
    //获取图形验证码
    void getCode(HttpServletRequest req, HttpServletResponse resp);
    //将String[]转换成String,并用','分割
    String splitStringArr(String[] arr);

}
