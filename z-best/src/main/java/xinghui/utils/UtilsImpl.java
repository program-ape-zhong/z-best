package xinghui.utils;



//import com.aliyun.tea.TeaException;

import cn.dsna.util.images.ValidateCode;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.tea.TeaException;

import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;

public class UtilsImpl implements UtilsApi {

    /*
             * 执行sql语句
         * 注意：该方法不提供关闭数据库资源(connection)
         *     查看当前活动的资源连接数：
               SHOW STATUS LIKE 'Threads_connected';
         * @param: sql 			执行的sql语句
         * @param: doing 		执行的类型(into 插入，query 查询)
         * @return: ResultSet 	返回数据库查询结果集对象
         * */
	@Override
	public MysqlOnlineObject insertDate(String sql, String doing) {
		
		Statement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;
        //获取工具类
        connection = MysqlConnection.getConnection();
        try {
            //获取数据库执行对象
            statement = connection.createStatement();
            //执行数据库语句
            //INSERT INTO account(userName,userAge, userSex) VALUES();
            switch (doing) {
			case "into":
				statement.execute(sql);
				break;
			case "query":
				resultSet =  statement.executeQuery(sql);
				break;
			default:
				System.out.println("doing参数错误，无法执行sql语句");
				break;
			}
        } catch (SQLException e) {
            System.out.println("insertDate数据添加失败");
            e.printStackTrace();
        }
//        System.out.println("insertDate数据添加成功");
		return new MysqlOnlineObject(connection,statement, resultSet);
	}


	@Override //TODO 仅供参考
    public String getClientIP(HttpServletRequest request) {
//        String clientIP = request.getHeader("X-Forwarded-For");
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        // 获取到多个ip时取第一个作为客户端真实ip  TODO 无法导包
//        if (StringUtils(ip) && ip.contains(",")) {
//            String[] ipArray = ip.split(",");
//            if (ArrayUtils.isNotEmpty(ipArray)) {
//                ip = ipArray[0];
//            }
//        }
        System.out.println("getClientIP获取成功：" + ip);
        return ip;
    }

    /*
      通过cookie名称删除或查询cookie
    * @param request 请求头
    * @param response 响应头
    * @param cookieName 要删除的cookie名称
    * @param doing     要执行的操作：delete->删除cookie query->查询cookie是否存在
    * @return (String)返回cookie是否存在
    * */
    @Override
    public String doingCookieByName(HttpServletRequest request, HttpServletResponse response, String cookieName,String doing) {
        // 获取客户端的所有 Cookie
        Cookie[] cookies = request.getCookies();
        String noting = "未开启自动登录";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                // 查找要删除的 Cookie
                if (cookie.getName().equals(cookieName)) {
                    switch (doing){
                        case "delete":
                            // 将 Cookie 的最大寿命设置为 0，以删除它
                            cookie.setMaxAge(0);
                            // 将响应发送给客户端，告诉它删除该 Cookie
                            response.addCookie(cookie);
                            System.out.println("deleteCookieByName删除成功");
                            break;
                        case  "query":
                            noting = "已开启自动登录";
                            return noting;
                    }
                }
            }
        }
        try {

            noting = URLEncoder.encode(noting,"UTF-8");
        }catch (Exception e){
            System.out.println(e);
        }
        return noting;
    }

    /*
    通过session创建cookie
    * @param req 请求头
    * @param resp 响应头
    * @param session 通过session会话创建cookie
    * @param attribute 获取session.getAttribute("")的参数
    * @return ck 返回Cookie对象
    * */
	@Override
    public Cookie createCode(HttpServletRequest req, HttpServletResponse resp, HttpSession session,String attribute) {
        //创建Cookie 将accountId写入cookie中
        //遍历cookie
        Cookie[] cks = Objects.equals(req.getCookies(), null) ? null : req.getCookies();
        //遍历cookie
        boolean noting = false;
        if (!Objects.equals(cks, null)) {
            for (Cookie ck : cks) {
                System.out.println(ck.getName());
                if (ck.getName().equals("code")) {
                    noting = true;
                    break;
                }
            }
        }
        //创建cookie
        Cookie ck = null;
        if (!noting) {
            ck = new Cookie("code", (String) session.getAttribute(attribute));
            //设置Cookie路径
            ck.setPath("/");
            //内存存储 一个自然月 30天
            ck.setMaxAge(2592000);
            resp.addCookie(ck);
        }
        System.out.println("createCode创建成功");
        return ck;
    }  

    /*
    * 密码加密
    * @param phones 传入的密码
    * return before+"****"+after 返回加密后的密码
    * */
    @Override
    public String setPassword(String phones) {
		int num = phones.length()/3;
		String before = phones.substring(0,num);
		String after = phones.substring(num*2);
		return before+"****"+after;
	}

    /**
     * 将获取的图片对象转换成base64格式
     * @param part 文件对象
     * @return base64编码格式
     */
    @Override
    public String getBase64ByPart(Part part) {
        String base64Img = "";
        InputStream is = null;
        try {
            is = part.getInputStream();
            int fileLength = is.available();
            byte[] imgDate = new byte[fileLength];
            is.read(imgDate);
            base64Img = "data:image/png;base64,"+ Base64.getEncoder().encodeToString(imgDate);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return base64Img;
    }

    /**
     * 判断字符串是否为空
     * @param str
     * @return 为空或空字符串返回true,否则返回false
     */
    public boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    /**
     * 判断日期格式是否正确
         * 详情：
         * 在这个例子中，我们定义了一个isValidFormat方法，它接受一个日期格式（如"yyyy-MM-dd"）和一个字符串。
         * 然后，我们尝试使用SimpleDateFormat的parse方法将字符串解析为日期。如果解析成功，并且原始字符串和解析后的日期字符串相同，
         * 那么我们就可以认为这个字符串是一个有效的日期格式。如果解析失败或者日期字符串和原始字符串不同，那么我们就认为这个字符串不是一个有效的日期格式。
     * @param format 格式 如：yyyy-MM-dd
     * @param value 需要判断的字符串 如：2023-07-30
     * @return 返回结果 true为格式正确，false为错误
     */
    public boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }

    /**
     * 获取图形验证码
     * @param req 请求
     * @param resp 响应
     * @throws IOException Io流
     */
    public void getCode(HttpServletRequest req, HttpServletResponse resp){
        //        创建验证码图片
        ValidateCode code = new ValidateCode(200, 30, 4, 20); //宽 高 验证码长度 干扰线数量
//        验证码图片响应给客户端
        try {
            code.write(resp.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
//        获取随机生成的code字符
        String codes = code.getCode();
        HttpSession session = req.getSession();
//        传值
        session.setAttribute("code",codes);
        System.out.println("验证码为："+ codes);
    }

    /**
     * 将数组中的字符串按照','分割并转成字符串类型
     * @param arr 需要分割的数组
     * @return 返回一个字符串，并用','分割
     */
    @Override
    public String splitStringArr(String[] arr) { //[1,2,3]
        String s = "";
        for (int i = 0; i < arr.length; i++) {
            s += i!=arr.length-1?arr[i]+",":arr[i];
        }
        return s;
    }
}


