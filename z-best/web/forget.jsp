<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
	<head lang="en">
		<meta charset="utf-8" />
		<title>forget</title>
		<link rel="stylesheet" type="text/css" href="css/public.css"/>
		<link rel="stylesheet" type="text/css" href="css/proList.css" />
		<link rel="stylesheet" type="text/css" href="css/forget.css" />
		<script type="text/javascript" src="js/hint.js"></script>
	</head>
	<body>
	<div id="hint" style="z-index: 1">
		${hint==""?"您已进入账号找回页面":hint}
	</div>
		<!----------------------------------------order------------------>
		<div class="order cart">
			<!-----------------logo------------------->
			<div class="logo">
				<h1 class="wrapper clearfix">
					<a href="isLogin/index.jsp"><img class="fl" src="img/temp/logo.png"></a>
				</h1>
			</div>
			<div class="forCon">
				<p>安全设置-找回密码</p>
				<ul>
					<li class="on"><span>01/</span>输入登录名</li>
					<li><span>02/</span>验证信息</li>
					<li><span>03/</span>重置密码</li>
				</ul>
				<div class="formCon">
					<!--步骤1-->
					<form action="${ctp}/isLogin/AccountController?doing=findPassWord&page=1" method="post" class="one">
						<input type="text" required value="" placeholder="昵称/邮箱" id="inputPhone"><label>请输入手机号</label><br />
						<input type="text" required value="" placeholder="验证码"  id="inputCode"><label>请输入验证码</label><br />
						<img src="${pageContext.request.contextPath}/isLogin/AccountController?doing=getCode" class="code" id="img" alt="网络异常" onclick = changeImg()><br>
						<input type="button" value="下一步" class="next"> <!-- 属性不能设置为submit，否则会跳转页面！！！ -->
					</form>
					<!--步骤2-->
					<form action="#" method="post" class="two">
						<p>手机号：<span id="spanPhone"></span></p>
						<input type="text" required placeholder="请输入短信验证码" id="inputSMS">
						<input type="button" value="发送" id="sendSMS">
						<p class="tip" style="margin-top: 7px">短信验证码已发往您的移动设备，请输入验证码完成验证</p>
						<p id="hint2" style="color: #ff8a8a"></p>
						<input type="button" value="验证" class="next1" id="noting">
					</form>
					<!--步骤3-->
					<form action="#" method="post" class="three">
						<label>新密码：</label><input type="text" value="" id="newPassWord"><br />
						<label>确认密码：</label><input type="text" value="" id="newPassWord2"><br />
						<p id="hint3" style="color: #ff8a8a"></p>
						<input type="button" value="完成" id="submit">
					</form>
				</div>
			</div>
		</div>
		<!-- 导入axios库 -->
		<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?name"></script>
		<script language="javascript" type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
		<script>
			function changeImg() {
				document.getElementById("img").src = "${pageContext.request.contextPath}/isLogin/AccountController?doing=getCode&r=" + Math.random();
			}
			//点击第一个页面的按钮
				$(".next").click(function () {
					let inputPhone = document.getElementById("inputPhone").value;
					let inputCode = document.getElementById("inputCode").value;
					axios({
						url:'http://localhost:9090/z/isLogin/AccountController',
						params:{
							doing:"findPassWord",
							inputPhone:inputPhone,
							inputCode:inputCode
						}
					}).then(function(result){ //function(result) 可以用result=>{}代替
						let res = result.data+"";
						//显示第二个页面
						if (res.includes("true")){
							$(".two").show();
							$(".one").hide();
							$(".forCon ul li").eq(1).addClass("on").siblings("li").removeClass("on");
							document.getElementById("spanPhone").innerText = inputPhone;
						}else{
							changeImg();
							alert("账户或验证码错误，请重新输入")
						}
						console.log(result)
						//把数据转li标签插入到页面上
					})



			})
			let i = 1;
			let sendSMS = document.getElementById("sendSMS"); //点击发送按钮
			let inputSMS = document.getElementById("inputSMS"); //用户输入的验证码
			let noting = document.getElementById("noting"); //提交按钮
			let hint2 = document.getElementById("hint2");
			let sms = null;
			//点击第二个页面的按钮


				$("#sendSMS").click(function () {
					if (sendSMS.value==="发送"){
						alert("发送成功,请及时接收")
						i = 1;
						axios({
							url:'http://localhost:9090/z/isLogin/AccountController',
							params:{
								doing:"sendSMS"
							}
						}).then(function(result){ //接收验证码
							timeDown();
							sms = result.data;
						})
					}
				})
			//提交时判断短信验证码是否正确

				noting.addEventListener("click",function (){
					console.log("sendSMS.value="+sendSMS.value)
					if (inputSMS.value==sms&&sendSMS.value !== "发送"){
					//展示第三个页面
					$(".two").hide();
					$(".one").hide();
					$(".three").show();
					$(".forCon ul li").eq(2).addClass("on").siblings("li").removeClass("on");
					}else if (sendSMS.value === "发送"){
						hint2.innerText = "验证码超时，请重新获取"
					} else if (inputSMS.value!=sms){
						hint2.innerText = "验证码错误，请重新输入"
					}
				})
			//提交新密码
			let inputPhone = document.getElementById("inputPhone");//手机号
			let newPassWord = document.getElementById("newPassWord"); //新密码
			let newPassWord2 = document.getElementById("newPassWord2");//新密码
			let hint3 = document.getElementById("hint3");//第三页的提示


			newPassWord.addEventListener("blur",function (){
				if (newPassWord.value !== newPassWord2.value){
					hint3.innerText = "两次输入的密码不同，请及时修改"
				}else {
					hint3.innerText = ""
				}
			})
			newPassWord2.addEventListener("blur",function (){
				if (newPassWord.value !== newPassWord2.value){
					hint3.innerText = "两次输入的密码不同，请及时修改"
				}else {
					hint3.innerText = ""
				}
			})

			let submit = document.getElementById("submit");//提交
			submit.addEventListener("click",function (){
				if (newPassWord.value === newPassWord2.value){
					hint3.innerText = ""
					axios({
						method:"get",
						url:"http://localhost:9090/z/isLogin/AccountController",
						params:{
							doing:"updatePassWordByPhone",
							inputPhone:inputPhone.value,
							newPassWord:newPassWord.value
						}
					}).then(function (resp) {
						if (resp.data===true){
						window.location.href = 'http://localhost:9090/z/login.jsp'
						}else {
							hint3.innerText = resp.data;
						}
					})
				}else {
					hint3.innerText = "两次输入的密码不同，请及时修改"
				}
			})
				function timeDown() {
					sendSMS.value = 60 - i;
					console.log(i)
					i++;
					if (sendSMS.value!=="0"){
						setTimeout(timeDown,1000);
					}else {
						sendSMS.value = "发送"
					}
			}
		</script>
		<script src="js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/public.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/pro.js" type="text/javascript" charset="utf-8"></script>
	</body>
</html>
