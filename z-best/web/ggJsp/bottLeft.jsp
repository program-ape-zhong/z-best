<%--
  Created by IntelliJ IDEA.
  User: 星辉
  Date: 2023/12/6
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="zuo fl">
    <h3>
        <a href="#"><img src="${account.base64Img}" width="92" height="92"/></a>
        <p class="clearfix"><span class="fl">${account.name}</span><a href="${ctp}/login.jsp" class="fr">[退出登录]</a></p>
    </h3>
    <div>
        <h4>我的交易</h4>
        <ul>
            <li><a id="wdgwc" href="/z/isLogin/cart.jsp">我的购物车</a></li>
            <li><a id="wddd" href="myorderq.html?liSelect=wddd">我的订单</a></li>
            <li><a id="pjsd" href="myprod.html?liSelect=pjsd">评价晒单</a></li>
        </ul>
        <h4>个人中心</h4>
        <ul>
            <li><a id="wdzx" href="mygxin.jsp?liSelect=wdzx">我的中心</a></li>
            <li><a id="dzgl" href="address.jsp?liSelect=dzgl">地址管理</a></li>
        </ul>
        <h4>账户管理</h4>
        <ul>
            <li><a id="grxx" href="mygrxx.jsp?liSelect=grxx">个人信息</a></li>
            <li><a id="xgmm" href="remima.jsp?liSelect=xgmm">修改密码</a></li>
        </ul>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        // 在这里执行您的 JavaScript 代码
        document.getElementById("${param.liSelect}").style.color = '#A10000';
        document.getElementById("${param.liSelect}").style.fontWeight = '600';
    });

</script>
</body>
</html>
