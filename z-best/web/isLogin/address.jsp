<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %> <!-- 1、编码 -->
<!DOCTYPE html>
<html>
	<head lang="en">
		<meta charset="utf-8" />
		<title>最家</title>
		<link rel="stylesheet" type="text/css" href="../css/public.css"/>
		<link rel="stylesheet" type="text/css" href="../css/mygxin.css" />
		<script type="text/javascript" src="../js/hint.js"></script> <!-- 2、jsHint -->
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  <!-- 3、JSTL -->
<%--		<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?name"></script> --%>
		<script src="../js/axios-0.18.0.js"></script><!-- axios -->
	</head>
	<body>
	<div id="hint" style="z-index: 999"> <!-- 4、divHint -->
		${hint==""?"您已进入修改页面":hint}
		${sessionScope.remove("hint")}
	</div>
	<jsp:include page="../ggJsp/minHead.jsp"/> <!-- 5、minHead -->
		<!------------------------------idea------------------------------>
		<div class="address mt">
			<div class="wrapper clearfix">
				<a href="index.html" class="fl">首页</a>
				<span>/</span>
				<a href="mygxin.html">个人中心</a>
				<span>/</span>
				<a href="address.jsp" class="on">地址管理</a>
			</div>
		</div>
		
		<!------------------------------Bott------------------------------>
		<div class="Bott">
			<div class="wrapper clearfix">
				<jsp:include page="../ggJsp/bottLeft.jsp"/> <!-- 6、bottLeft -->
				<div class="you fl">
					<h2>收货地址</h2>
					<div class="add add2" id="addDiv">
<%--						<div>--%>
<%--							<a href="#2" id="addxad"><img src="../img/jia.png"/></a>--%>
<%--							<span>添加新地址</span>--%>
<%--						</div>--%>
<%--						<div class="dizhi">--%>
<%--							<p>六六六1</p>--%>
<%--							<p>1573****666</p>--%>
<%--							<p>河北省 唐山市 路北区</p>--%>
<%--							<p>唐山市大学生公寓村（063000）</p>--%>
<%--						</div>--%>
					</div>
				</div>
			</div>
		</div>
		<!--编辑弹框-->
		<!--遮罩-->
		<div class="mask"></div>
		<div class="adddz">
			<form action="#" method="get" class="adddzForm">
				<input type="text" placeholder="姓名" class="on" id="name"/>
				<input type="text" placeholder="手机号" id="phone"/>
				<div class="city">
					<select name="" id="province">
						<option value="省份/自治区">省份/自治区</option>
					</select>
					<select id="city">
						<option value="城市/地区">城市/地区</option>
					</select>
					<select id="area">
						<option value="区/县">区/县</option>
					</select>
					<select id="sendYourArea">sendYourArea
						<option value="配送区域">配送区域</option>
						<option value="学校">学校</option>
						<option value="公司">公司</option>
						<option value="学生公寓">学生公寓</option>
						<option value="行政地区">行政地区</option>
					</select>
				</div>
				<textarea name="" rows="" cols="" placeholder="详细地址" id="territorialIntegrity"></textarea>
				<input type="text" placeholder="邮政编码" id="postalCode" />
				<div class="bc">
					<input type="button" value="保存" onclick="submitAccountArea()"/>
					<input type="button" value="取消" />
				</div>
			</form>
		</div>
		<div class="readd">
			<form action="#" method="get" >
				<input type="text"  class="on" value="六六六" />
				<input type="text" value="157****0022" />
				<div class="city">
					<select name="">
						<option value="省份/自治区">河北省</option>
					</select>
					<select>
						<option value="城市/地区">唐山市</option>
					</select>
					<select>
						<option value="区/县">路北区</option>
					</select>
					<select>
						<option value="配送区域">火炬路</option>
					</select>
				</div>
				<textarea name="" rows="" cols="" placeholder="详细地址">高新产业园</textarea>
				<input type="text" placeholder="邮政编码" value="063000"/>
				<div class="bc">
					<input type="button" value="保存" />
					<input type="button" value="取消" />
				</div>
			</form>
		</div>
		<!--返回顶部-->
		<div class="gotop">
			<a href="cart.html">
			<dl>
				<dt><img src="../img/gt1.png"/></dt>
				<dd>去购<br />物车</dd>
			</dl>
			</a>
			<a href="#" class="dh">
			<dl>
				<dt><img src="../img/gt2.png"/></dt>
				<dd>联系<br />客服</dd>
			</dl>
			</a>
			<a href="mygxin.html">
			<dl>
				<dt><img src="../img/gt3.png"/></dt>
				<dd>个人<br />中心</dd>
			</dl>
			</a>
			<a href="#" class="toptop" style="display: none">
			<dl>
				<dt><img src="../img/gt4.png"/></dt>
				<dd>返回<br />顶部</dd>
			</dl>
			</a>
			<p>XXX</p>
		</div>
		
		
		<!--footer-->
		<div class="footer">
			<div class="top">
				<div class="wrapper">
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot1.png"/></a>
						<span class="fl">7天无理由退货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot2.png"/></a>
						<span class="fl">15天免费换货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot3.png"/></a>
						<span class="fl">满599包邮</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot4.png"/></a>
						<span class="fl">手机特色服务</span>
					</div>
				</div>
			</div>
			<p class="dibu">最家家居&copy;2020公司版权所有 京ICP备XXX备XXX号<br />
			违法和不良信息举报电话：XXX，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</p>
		</div>
		<script src="../js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/public.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/user.js" type="text/javascript" charset="utf-8"></script>
		<script>
			//添加地址
			function submitAccountArea(){
				let accountArea = {
					accountId: ${account.id},
					name:document.getElementById("name").value,
					phone:document.getElementById("phone").value,
					province:province[province.selectedIndex].value,
					city:city[city.selectedIndex].value,
					area:area[area.selectedIndex].value,
					sendYourArea:document.getElementById("sendYourArea")[document.getElementById("sendYourArea").selectedIndex].value,
					territorialIntegrity:document.getElementById("territorialIntegrity").value,
					postalCode:document.getElementById("postalCode").value
				}
				console.log(accountArea)
				axios({
					//服务器地址
					method:"post",
					url:'http://localhost:9090/z/isLogin/AccountController',
					params:{
						doing:"addAccountArea",
						data:JSON.stringify(accountArea)
					}
				}).then(result =>{
					getAllAccountArea();
				})
			}
			getAllAccountArea();
			function getAllAccountArea(){
				axios({
					//服务器地址
					method:"get",
					url:'http://localhost:9090/z/isLogin/AccountController',
					params:{
						doing:"getAllAccountArea"
					}
				}).then(result =>{
					let addDiv = document.getElementById("addDiv");
					let data = result.data;
					let str = `<div id="sjwt">
							<a href="#2" id="addxad"><img src="../img/jia.png"/></a>
					<span>添加新地址</span>
				</div>`;
					for (let i = 0; i < data.length; i++) {
						str += "<div class=dizhi>";
						str += "<p>"+data[i].name+"</p>";
						str += "<p>"+data[i].phone+"</p>";
						str += "<p>"+data[i].province+data[i].city+data[i].area+data[i].sendYourArea+"</p>";
						str += "<p>"+data[i].territorialIntegrity+"（"+data[i].postalCode+"）</p>";
						str += "</div>";
						addDiv.innerHTML = str;
					}
					console.log(str)
				})
			}





			//获取省份
			let province = document.getElementById("province");
			axios({
				//服务器地址
				url:'https://hmajax.itheima.net/api/province', //province：省份，city：城市，area：地区
				// 传递查询参数 ->河北省的城市列表
			}).then(result =>{
				for (let i = 0; i < result.data.list.length; i++) {
					province.innerHTML += "<option value='"+result.data.list[i]+"'>"+result.data.list[i]+"</option>"
				}
			})
			//获取城市
			let city = document.getElementById("city");
			document.getElementById("province").addEventListener("change",function () {
				getCity(1)
			})
			//获取地区
			let area = document.getElementById("area");
			document.getElementById("city").addEventListener("change",function c() {
				getArea()
			})

			function getCity(num) { //num === 1 加载地区
				if (province[province.selectedIndex].innerText!=="省份/自治区"){
					axios({
						//服务器地址
						url:'https://hmajax.itheima.net/api/city',
						params:{
							pname:province[province.selectedIndex].innerText,
						}
						// 传递查询参数 ->河北省的城市列表
					}).then(result =>{
						city.innerHTML = "";
						for (let i = 0; i < result.data.list.length; i++) {
							city.innerHTML += "<option value='"+result.data.list[i]+"'>"+result.data.list[i]+"</option>"
						}
						if (num===1){
							getArea()
						}
					})
				}
			}

			function getArea(){
				console.log("选中的省份："+province[province.selectedIndex].innerText)
				console.log("选择的城市："+city[city.selectedIndex].innerText)
				if (city[city.selectedIndex].innerText!=="城市/地区"){
					axios({
						//服务器地址
						url:'https://hmajax.itheima.net/api/area',
						params:{
							pname:province[province.selectedIndex].innerText,
							cname:city[city.selectedIndex].innerHTML
						}
						// 传递查询参数 ->河北省的城市列表
					}).then(result =>{
						console.log(result.data)
						area.innerHTML = "";
						for (let i = 0; i < result.data.list.length; i++) {
							area.innerHTML += "<option value='"+result.data.list[i]+"'>"+result.data.list[i]+"</option>"
						}
					})
				}
			}
		</script>
	</body>
</html>
