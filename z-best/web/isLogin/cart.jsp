<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %> <!-- 1、编码 -->
<!DOCTYPE html>
<html>
	<head lang="en">
		<meta charset="utf-8" />
		<title>cart</title>
		<link rel="stylesheet" type="text/css" href="../css/public.css"/>
		<link rel="stylesheet" type="text/css" href="../css/proList.css" />
		<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?name"></script> <!-- axios -->
		<script type="text/javascript" src="../js/hint.js"></script> <!-- 2、jsHint -->
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  <!-- 3、JSTL -->
	</head>
	<script>
		//初始化当前商品
		axios({
			url:'http://localhost:9090/z/isLogin/GoodsController',
			params:{
				doing:"getGoodsCarOfAllGoods"
			}
		}).then(function(result){ //function(result) 可以用result=>{}代替
			if (${empty goodsCarOfAllGoods}){
				location.reload();
			}
		})
	</script>
	<body>
	<div id="hint" style="z-index: 999"> <!-- 4、divHint -->
		${hint==""?"您已进入购物车页面":hint}
		${sessionScope.remove("hint")}
	</div>
		<!--------------------------------------cart--------------------->
		<jsp:include page="/ggJsp/cartHead.jsp" />
		<div class="cart mt">
			<!-----------------logo------------------->
			<!--<div class="logo">
				<h1 class="wrapper clearfix">
					<a href="index.html"><img class="fl" src="img/temp/logo.png"></a>
					<img class="top" src="img/temp/cartTop01.png">
				</h1>
			</div>-->
			<!-----------------site------------------->
			<div class="site">
				<p class=" wrapper clearfix">
					<span class="fl">购物车</span>
					<img class="top" src="../img/temp/cartTop01.png">
					<a href="index.jsp" class="fr">继续购物&gt;</a>
				</p>
			</div>
			<!-----------------table------------------->
			<div class="table wrapper">
				<div class="tr">
					<div>商品</div>
					<div>单价</div>
					<div>数量</div>
					<div>小计</div>
					<div>操作</div>
				</div>
				<c:forEach items="${goodsCarOfAllGoods}" var="g">
					<div class="th">
						<div class="pro clearfix">
							<label class="fl">
								<input type="checkbox" name="payGoods" value="${g.id}"/>
								<span></span>
							</label>
							<a class="fl" href="#">
								<dl class="clearfix">
									<dt class="fl"><img src="${g.img}" width="120" height="120"></dt>
									<dd class="fl">
										<p>${g.name}</p>
										<p>颜色分类:</p>
										<p>${g.configurationText}</p>
									</dd>
								</dl>
							</a>
						</div>
						<div class="price">$${g.price}</div>
						<div class="number">
							<p class="num clearfix">
								<img class="fl sub" src="../img/temp/sub.jpg" value="${g.id}">
								<span class="fl">${g.count}</span>
								<img class="fl add" src="../img/temp/add.jpg" value="${g.id}">
							</p>
						</div>
						<div class="price sAll">$${g.sumPrice}</div>
						<div class="price"><a class="del" href="#2" value="${g.id}">删除</a></div>
					</div>
				</c:forEach>

				<div class="goOn">空空如也~<a href="index.html">去逛逛</a></div>
				<div class="tr clearfix">
					<label class="fl">
						<input class="checkAll" type="checkbox"/>
						<span></span>
					</label>
					<p class="fl">
						<a href="#">全选</a>
						<a href="#" class="del">删除</a>
					</p>
					<p class="fr">
						<span>共<small id="sl">0</small>件商品</span>
						<span>合计:&nbsp;<small id="all">$ 0.00</small></span>
						<a href="#" class="count" onclick="addOrder()">结算</a>
					</p>
				</div>
			</div>
		</div>
		<div class="mask"></div>
		<div class="tipDel">
			<p>确定要删除该商品吗？</p>
			<p class="clearfix">
				<a class="fl cer" href="#" id="true">确定</a>
				<a class="fr cancel" href="#">取消</a>
			</p>
		</div>
		<!--返回顶部-->
		<div class="gotop">
			<a href="cart.jsp">
			<dl>
				<dt><img src="../img/gt1.png"/></dt>
				<dd>去购<br />物车</dd>
			</dl>
			</a>
			<a href="#" class="dh">
			<dl>
				<dt><img src="../img/gt2.png"/></dt>
				<dd>联系<br />客服</dd>
			</dl>
			</a>
			<a href="mygxin.html">
			<dl>
				<dt><img src="../img/gt3.png"/></dt>
				<dd>个人<br />中心</dd>
			</dl>
			</a>
			<a href="#" class="toptop" style="display: none;">
			<dl>
				<dt><img src="../img/gt4.png"/></dt>
				<dd>返回<br />顶部</dd>
			</dl>
			</a>
			<p>XXX</p>
		</div>
		<!--footer-->
		<div class="footer">
			<div class="top">
				<div class="wrapper">
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot1.png"/></a>
						<span class="fl">7天无理由退货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot2.png"/></a>
						<span class="fl">15天免费换货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot3.png"/></a>
						<span class="fl">满599包邮</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot4.png"/></a>
						<span class="fl">手机特色服务</span>
					</div>
				</div>
			</div>
			<p class="dibu">最家家居&copy;2020公司版权所有 京ICP备XXX备XXX号<br />
			违法和不良信息举报电话：188-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</p>
		</div>
		<!----------------mask------------------->
		<div class="mask"></div>
		<!-------------------mask内容------------------->
		<div class="proDets">
			<img class="off" src="../img/temp/off.jpg" />
			<div class="proCon clearfix">
				<div class="proImg fr">
					<img class="list" src="../img/temp/proDet.jpg"  />
					<div class="smallImg clearfix">
						<img src="../img/temp/proDet01.jpg" data-src="img/temp/proDet01_big.jpg">
						<img src="../img/temp/proDet02.jpg" data-src="img/temp/proDet02_big.jpg">
						<img src="../img/temp/proDet03.jpg" data-src="img/temp/proDet03_big.jpg">
						<img src="../img/temp/proDet04.jpg" data-src="img/temp/proDet04_big.jpg">
					</div>
				</div>
				<div class="fl">
					<div class="proIntro change">
						<p>颜色分类</p>
						<div class="smallImg clearfix">
							<p class="fl on"><img src="../img/temp/prosmall01.jpg" alt="白瓷花瓶+20支快乐花" data-src="img/temp/proBig01.jpg"></p>
							<p class="fl"><img src="../img/temp/prosmall02.jpg" alt="白瓷花瓶+20支兔尾巴草" data-src="img/temp/proBig02.jpg"></p>
							<p class="fl"><img src="../img/temp/prosmall03.jpg" alt="20支快乐花" data-src="img/temp/proBig03.jpg"></p>
							<p class="fl"><img src="../img/temp/prosmall04.jpg" alt="20支兔尾巴草" data-src="img/temp/proBig04.jpg"></p>
						</div>
					</div>
					<div class="changeBtn clearfix">
						<a href="#2" class="fl"><p class="buy">确认</p></a>
						<a href="#2" class="fr"><p class="cart">取消</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="pleaseC">
			<p>请选择宝贝</p>
			<img class="off" src="../img/temp/off.jpg" />
		</div>
		<script src="../js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/public.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/pro.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/cart.js" type="text/javascript" charset="utf-8"></script>
		<script>
			function addOrder(){
				let arr = "";
				for (let i = 0; i < document.getElementsByName("payGoods").length; i++) {
					let ee = document.getElementsByName("payGoods")[i];
					if (ee.checked === true){
						arr += i<document.getElementsByName("payGoods").length?ee.value+",":ee.value;
					}
				}
				//创建订单
				axios({
					url:'http://localhost:9090/z/isLogin/GoodsController',
					params:{
						doing:"addOrder",
						id:arr
					}
				}).then(function(result){ //function(result) 可以用result=>{}代替
					window.location.href = 'http://localhost:9090/z/isLogin/order.jsp'
				})
			}






			//-单个商品
			for (const x of document.getElementsByClassName("sub")) {
				x.addEventListener("click",function () {
					axios({
						url:'http://localhost:9090/z/isLogin/GoodsController',
						params:{
							doing:"updateGoodsCarCount",
							doing2:"sub",
							id:x.attributes[2].nodeValue //商品id
						}
					}).then(function(result){ //function(result) 可以用result=>{}代替
					})
				})
			}
			//+单个商品
			for (const x of document.getElementsByClassName("add")) {
				x.addEventListener("click",function () {
					axios({
						url:'http://localhost:9090/z/isLogin/GoodsController',
						params:{
							doing:"updateGoodsCarCount",
							doing2:"add",
							id:x.attributes[2].nodeValue //商品id
						}
					}).then(function(result){ //function(result) 可以用result=>{}代替
					})
				})
			}
			//删除购物车商品
			for (const x of document.getElementsByClassName("del")) {
				x.addEventListener("click",function () {
					document.getElementById("true").addEventListener("click",function (){
						axios({
							url:'http://localhost:9090/z/isLogin/GoodsController',
							params:{
								doing:"deleteGoodsCarOfGoods",
								id:x.attributes[2].nodeValue //商品id
							}
						}).then(function(result){ //function(result) 可以用result=>{}代替
						})
					})
				})
			}
		</script>
	</body>
</html>
