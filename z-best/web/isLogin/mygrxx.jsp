<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %> <!-- 1、编码 -->
<!DOCTYPE html>
<html>
	<head lang="en">
		<meta charset="utf-8" />
		<title>个人信息</title>
		<link rel="stylesheet" type="text/css" href="../css/public.css"/>
		<link rel="stylesheet" type="text/css" href="../css/mygrxx.css" />
		<script type="text/javascript" src="../js/hint.js"></script> <!-- 2、jsHint -->
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  <!-- 3、JSTL -->
	</head>
	<body>
	<div id="hint" style="z-index: 999"> <!-- 4、divHint -->
		${hint==""?"您已进入修改页面":hint}
		${sessionScope.remove("hint")}
	</div>
	<jsp:include page="../ggJsp/minHead.jsp"/> <!-- 5、minHead -->

<%--		<!------------------------------idea------------------------------>--%>
		<div class="address mt">
			<div class="wrapper clearfix">
				<a href="index.html" class="fl">首页</a>
				<span>/</span>
				<a href="mygxin.html" class="on">个人信息</a>
			</div>
		</div>
		
		<!------------------------------Bott------------------------------>
		<div class="Bott">
			<div class="wrapper clearfix">
				<jsp:include page="../ggJsp/bottLeft.jsp"/> <!-- 6、bottLeft -->
				<div class="you fl">
					<h2>个人信息</h2>
					<div class="gxin">
						<div class="tx"><a href="#"><img src="${account.base64Img}" width="92" height="92"/><p id="avatar">修改头像</p></a></div>
						<div class="xx">
							<h3 class="clearfix"><strong class="fl">基础资料</strong><a href="#" class="fr" id="edit1">编辑</a></h3>
							<div>姓名：${account.name}</div>
							<div>生日：${account.birthday}</div>
							<div>性别：${account.sex}</div>
							<h3>高级设置</h3>
							<!--<div><span class="fl">银行卡</span><a href="#" class="fr">管理</a></div>-->
							<div><span class="fl">账号地区：${account.region}</span><a href="#" class="fr" id="edit2">修改</a></div>
							<button type="button" id="addGoods">添加商品</button>
						</div>
					</div>			
				</div>
			</div>
		</div>
<%--	添加商品	--%>
		<div class="goodsDiv" id="goodsDiv">
			<form action="${goodsPath}?doing=addGoods" method="post" enctype="multipart/form-data" >
				商品名称：<input type="text" name="name" required ><br>
				商品详情：<input type="text" name="details" required ><br>
				商品价格：<input type="number" name="price" step="0.01"  required ><br>
				商品配置图片：上传<input type="radio" name="a" onclick="onConfigurationImg()">	网页路径<input type="radio" name="a" onclick="onConfigurationImg2()">		<br>
				<input type="file" name="configurationImg" multiple="multiple" id="configurationImg" style="display: none"><br>
				<input type="text" name="configurationImg2"  id="configurationImg2" placeholder="多个用',分割'" style="display: none"><br>
				商品配置描述：<input type="text" name="configurationText" placeholder="多个用','分割"><br>
				商品图片：<input type="text" name="img" required ><br>
				库存：<input type="number" name="count" required ><br>
				类型:<input type="text" name="type" required><br>
				<button type="submit">提交</button>
				<button type="button" onclick="noneAddGoods()">取消</button>
			</form>
		</div>
		<!--遮罩-->
		<div class="mask"></div>
		<!--编辑弹框-->
		<div class="bj">
			<div class="clearfix"><a href="#" class="fr gb"><img src="../img/icon4.png"/></a></div>
			<h3>编辑基础资料</h3>
			<form action="${ctp}/isLogin/AccountController?doing=updateInformation" method="post">
				<p><label>姓名：</label><input type="text" name="name" value="<c:if test="${account.name!=\"未命名\"}">${account.name}</c:if>" /></p>
				<p><label>生日：</label><input type="text" name="birthday" placeholder="请输入 yyyy-mm-dd 格式" value="<c:if test="${account.birthday!=\"未知\"}">${account.birthday}</c:if>" /></p>
				<p>
					<label>性别：</label>
					<span><input type="radio" name="sex" value="男" <c:if test="${account.sex==\"男\"}">checked</c:if>/>男</span>
					<span><input type="radio" name="sex" value="女"  <c:if test="${account.sex==\"女\"}">checked</c:if>/>女</span>
				</p>
				<div class="bc">
					<input type="submit" value="保存"  />
					<input type="button" value="取消" />
				</div>
			</form>
		</div>
		<!--高级设置修改-->
		<div class="xg">
			<div class="clearfix"><a href="#" class="fr gb"><img src="../img/icon4.png"/></a></div>
			<h3>切换账号地区</h3>
			<form action="${accountPath}?doing=updateRegion" method="post">
				<p><label>地区：</label><input type="text" name="region" value="${account.region}" /></p>
				<div class="bc">
					<input type="submit" value="保存" />
					<input type="button" value="取消" />
				</div>
			</form>
		</div>
		<!--修改头像-->
		<div class="avatar">
			<div class="clearfix"><a href="#" class="fr gb"><img src="../img/icon4.png"/></a></div>
			<h3>修改头像</h3>
			<form action="${accountPath}?doing=updateImg" enctype="multipart/form-data" method="post" >
				<h4>请上传图片</h4>
				<input type="file" value="上传头像" name="img" accept="image/*"/>
				<p>jpg或png，大小不超过12M</p>
				<input type="submit" value="提交" />
			</form>
		</div>
		
		<!--返回顶部-->
		<div class="gotop">
			<a href="cart.jsp">
			<dl>
				<dt><img src="../img/gt1.png"/></dt>
				<dd>去购<br />物车</dd>
			</dl>
			</a>
			<a href="#" class="dh">
			<dl>
				<dt><img src="../img/gt2.png"/></dt>
				<dd>联系<br />客服</dd>
			</dl>
			</a>
			<a href="mygxin.html">
			<dl>
				<dt><img src="../img/gt3.png"/></dt>
				<dd>个人<br />中心</dd>
			</dl>
			</a>
			<a href="#" class="toptop" style="display: none">
			<dl>
				<dt><img src="../img/gt4.png"/></dt>
				<dd>返回<br />顶部</dd>
			</dl>
			</a>
			<p>XXX</p>
		</div>
		<!--footer-->
		<div class="footer">
			<div class="top">
				<div class="wrapper">
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot1.png"/></a>
						<span class="fl">7天无理由退货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot2.png"/></a>
						<span class="fl">15天免费换货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot3.png"/></a>
						<span class="fl">满599包邮</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot4.png"/></a>
						<span class="fl">手机特色服务</span>
					</div>
				</div>
			</div>
			<p class="dibu">最家家居&copy;2020公司版权所有 京ICP备XXX备XXX号<br />
			违法和不良信息举报电话：XXX，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</p>
		</div>
		<script src="../js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/public.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/user.js" type="text/javascript" charset="utf-8"></script>
		<script>
			document.getElementById("addGoods").addEventListener("click",function (){
				document.getElementById("goodsDiv").style.display = "block";
			})
			function noneAddGoods() {
				document.getElementById("goodsDiv").style.display = "none";
			}
			function onConfigurationImg(){
				document.getElementById("configurationImg").style.display = "block";
				document.getElementById("configurationImg2").style.display = "none";
			}
			function onConfigurationImg2(){
				document.getElementById("configurationImg").style.display = "none";
				document.getElementById("configurationImg2").style.display = "block";
			}
		</script>
	</body>
</html>
