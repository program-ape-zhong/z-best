<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %> <!-- 1、编码 -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>详情页</title>
		<link rel="stylesheet" type="text/css" href="../css/public.css"/>
		<link rel="stylesheet" type="text/css" href="../css/proList.css"/>
		<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js?name"></script> <!-- axios -->
		<script type="text/javascript" src="../js/hint.js"></script> <!-- 2、jsHint -->
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  <!-- 3、JSTL -->
	</head>
	<script>
		//初始化当前商品
		axios({
			url:'http://localhost:9090/z/isLogin/GoodsController',
			params:{
				doing:"getGoods",
				id:${param.id}
			}
		}).then(function(result){ //function(result) 可以用result=>{}代替

		})
		if(${empty goods.name}) {
			location.reload(); //刷新页面
		}else if ((${empty goods.id?0:goods.id})!==(${param.id})){
			location.reload(); //刷新页面
		}
		//初始化'为您推荐'
		axios({
			url:'http://localhost:9090/z/isLogin/GoodsController',
			params:{
				doing:"getRecommendGoodsList",
			}
		}).then(function(result){ //function(result) 可以用result=>{}代替
		//初始化'猜您喜欢'
		})
		axios({
			url:'http://localhost:9090/z/isLogin/GoodsController',
			params:{
				doing:"getLoveGoodsList",
				type:'${empty goods.type?"装饰摆件":goods.type}'
			}
		}).then(function(result){ //function(result) 可以用result=>{}代替

		})
	</script>
	<body>
	<div id="hint" style="z-index: 999"> <!-- 4、divHint -->
		${hint==""?"您已进入商品详情页面":hint}
		${sessionScope.remove("hint")}
	</div>
	<!-----------------------head------------------------->
	<jsp:include page="/ggJsp/indexHead.jsp" />
		<!-----------------address------------------------------->
		<div class="address">
			<div class="wrapper clearfix">
				<a href="#">首页</a>
				<span>/</span>
				<a href="#">${sessionScope.goods.type}</a>
				<span>/</span>
				<a href="#">${sessionScope.goods.name}</a>
			</div>
		</div>
		<!-----------------------Detail------------------------------>
		<div class="detCon">
			<div class="proDet wrapper">
				<div class="proCon clearfix">
					<div class="proImg fl">
						<img class="det" src="${goods.img[0]}" />
						<div class="smallImg clearfix">
							<c:forEach items="${goods.configurationImg}" var="g" >
								<img src="${g}" data-src="${g}">
							</c:forEach>
						</div>
					</div>
					<div class="fr intro">
						<div class="title">
							<h4 id="h4">${goods.name}</h4>
							<p>${goods.details}</p>
							<span>￥${goods.price}</span>
						</div>
						<div class="proIntro">
							<p>颜色分类</p>
							<div class="smallImg clearfix categ">
								<c:forEach items="${goods.configurationImg}" var="g" varStatus="status">
									<p class="fl"><img src="${g}" id="a" alt="${goods.configurationText[status.index]}" value="${status.index}" data-src="${g}"></p>
								</c:forEach>
							</div>
							<p>数量&nbsp;&nbsp;库存<span>${goods.count}</span>件</p>
							<div class="num clearfix">
								<img class="fl sub" src="../img/temp/sub.jpg">
								<span class="fl" contentEditable="true" id="count">1</span>
								<img class="fl add" src="../img/temp/add.jpg">
								<p class="please fl">请选择商品属性!</p>
							</div>
						</div>
						<div class="btns clearfix">
							<a><p class="buy fl" id="addAGoodsCar" onclick="addGoodsCar(1)">立即购买</p></a>
							<a href="#2"><p class="cart fr" id="addGoodsCar" onclick="addGoodsCar()">加入购物车</p></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="introMsg wrapper clearfix">
			<div class="msgL fl">
				<div class="msgTit clearfix">
					<a class="on">商品详情</a>
					<a>所有评价</a>
				</div>
				<div class="msgAll">
					<div class="msgImgs">
						<c:forEach items="${goods.configurationImg}" var="g">
							<img src="${g}" width="942" height="1000"	>
						</c:forEach>
						<c:forEach items="${goods.img}" var="g">
							<img src="${g}" width="942" height="1000">
						</c:forEach>
					</div>
					<div class="eva">
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per01.jpg">
							<div class="perR fl">
								<p>馨***呀</p>
								<p>不好意思评价晚了，产品很好，价格比玻璃品便宜，没有我担心的杂色，发货快，包装好，全5分</p>
								<div class="clearfix">
									<p><img src="../img/temp/eva01.jpg"></p>
									<p><img src="../img/temp/eva02.jpg"></p>
									<p><img src="../img/temp/eva03.jpg"></p>
									<p><img src="../img/temp/eva04.jpg"></p>
									<p><img src="../img/temp/eva05.jpg"></p>
								</div>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per02.jpg">
							<div class="perR fl">
								<p>么***周</p>
								<p>花瓶超级棒，我看图以为是光面的，收货发现是磨砂，但感觉也超有质感，很喜欢。磨砂上面还有点纹路，不过觉得挺自然的，不影响美观。包装也很好，绝对不会磕碎碰坏，好评！</p>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per01.jpg">
							<div class="perR fl">
								<p>馨***呀</p>
								<p>不好意思评价晚了，产品很好，价格比玻璃品便宜，没有我担心的杂色，发货快，包装好，全5分</p>
								<div class="clearfix">
									<p><img src="../img/temp/eva01.jpg"></p>
									<p><img src="../img/temp/eva02.jpg"></p>
									<p><img src="../img/temp/eva03.jpg"></p>
									<p><img src="../img/temp/eva04.jpg"></p>
									<p><img src="../img/temp/eva05.jpg"></p>
								</div>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per02.jpg">
							<div class="perR fl">
								<p>么***周</p>
								<p>花瓶超级棒，我看图以为是光面的，收货发现是磨砂，但感觉也超有质感，很喜欢。磨砂上面还有点纹路，不过觉得挺自然的，不影响美观。包装也很好，绝对不会磕碎碰坏，好评！</p>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per01.jpg">
							<div class="perR fl">
								<p>馨***呀</p>
								<p>不好意思评价晚了，产品很好，价格比玻璃品便宜，没有我担心的杂色，发货快，包装好，全5分</p>
								<div class="clearfix">
									<p><img src="../img/temp/eva01.jpg"></p>
									<p><img src="../img/temp/eva02.jpg"></p>
									<p><img src="../img/temp/eva03.jpg"></p>
									<p><img src="../img/temp/eva04.jpg"></p>
									<p><img src="../img/temp/eva05.jpg"></p>
								</div>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per02.jpg">
							<div class="perR fl">
								<p>么***周</p>
								<p>花瓶超级棒，我看图以为是光面的，收货发现是磨砂，但感觉也超有质感，很喜欢。磨砂上面还有点纹路，不过觉得挺自然的，不影响美观。包装也很好，绝对不会磕碎碰坏，好评！</p>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per01.jpg">
							<div class="perR fl">
								<p>馨***呀</p>
								<p>不好意思评价晚了，产品很好，价格比玻璃品便宜，没有我担心的杂色，发货快，包装好，全5分</p>
								<div class="clearfix">
									<p><img src="../img/temp/eva01.jpg"></p>
									<p><img src="../img/temp/eva02.jpg"></p>
									<p><img src="../img/temp/eva03.jpg"></p>
									<p><img src="../img/temp/eva04.jpg"></p>
									<p><img src="../img/temp/eva05.jpg"></p>
								</div>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per02.jpg">
							<div class="perR fl">
								<p>么***周</p>
								<p>花瓶超级棒，我看图以为是光面的，收货发现是磨砂，但感觉也超有质感，很喜欢。磨砂上面还有点纹路，不过觉得挺自然的，不影响美观。包装也很好，绝对不会磕碎碰坏，好评！</p>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per01.jpg">
							<div class="perR fl">
								<p>馨***呀</p>
								<p>不好意思评价晚了，产品很好，价格比玻璃品便宜，没有我担心的杂色，发货快，包装好，全5分</p>
								<div class="clearfix">
									<p><img src="../img/temp/eva01.jpg"></p>
									<p><img src="../img/temp/eva02.jpg"></p>
									<p><img src="../img/temp/eva03.jpg"></p>
									<p><img src="../img/temp/eva04.jpg"></p>
									<p><img src="../img/temp/eva05.jpg"></p>
								</div>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
						<div class="per clearfix">
							<img class="fl" src="../img/temp/per02.jpg">
							<div class="perR fl">
								<p>么***周</p>
								<p>花瓶超级棒，我看图以为是光面的，收货发现是磨砂，但感觉也超有质感，很喜欢。磨砂上面还有点纹路，不过觉得挺自然的，不影响美观。包装也很好，绝对不会磕碎碰坏，好评！</p>
								<p><span>2016年12月27日08:31</span><span>颜色分类：大中小三件套（不含花）</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="msgR fr">
				<h4>为你推荐</h4>
				<div class="seeList">
					<c:forEach items="${RecommendGoodsList}" var="r">
						<a href="${ctp}/isLogin/proDetail.jsp?id=${r.id}">
							<dl>
								<dt><img src="${r.img[0]}" width="161" height="243"></dt>
								<dd>${r.name}</dd>
								<dd>￥${r.price}</dd>
							</dl>
						</a>
					</c:forEach>
				</div>
				
			</div>
		</div>
		<div class="like">
			<h4>猜你喜欢</h4>
			<div class="bottom">
				<div class="hd">
					<span class="prev"><img src="../img/temp/prev.png"></span>
					<span class="next"><img src="../img/temp/next.png"></span>
				</div>
				<div class="imgCon bd">
					<div class="likeList clearfix">
						<div>
							<c:forEach begin="0" end="4" varStatus="status" >
								<a href="proDetail.jsp">
									<dl>
										<dt><img src="${LoveGoodsList.get(status.index).img[0]}"></dt>
										<dd>${LoveGoodsList.get(status.index).name}</dd>
										<dd>￥${LoveGoodsList.get(status.index).price}</dd>
									</dl>
								</a>
							</c:forEach>
						</div>
						<div>
							<c:forEach  begin="5" end="${5}" varStatus="s" >
								<a href="proDetail.jsp">
									<dl>
										<dt><img src="${LoveGoodsList.get(s.index).img[0]}"></dt>
										<dd>${LoveGoodsList.get(s.index).name}</dd>
										<dd>￥${LoveGoodsList.get(s.index).price}</dd>
									</dl>
								</a>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--返回顶部-->
		<div class="gotop">
			<a href="cart.jsp">
			<dl class="goCart">
				<dt><img src="../img/gt1.png"/></dt>
				<dd>去购<br />物车</dd>
				<span>${sessionScope.account.goodsCarCount}</span>
			</dl>
			</a>
			<a href="#" class="dh">
			<dl>
				<dt><img src="../img/gt2.png"/></dt>
				<dd>联系<br />客服</dd>
			</dl>
			</a>
			<a href="mygxin.html">
			<dl>
				<dt><img src="../img/gt3.png"/></dt>
				<dd>个人<br />中心</dd>
			</dl>
			</a>
			<a href="#" class="toptop" style="display: none;">
			<dl>
				<dt><img src="../img/gt4.png"/></dt>
				<dd>返回<br />顶部</dd>
			</dl>
			</a>
			<p>XXX</p>
		</div>
		<div class="msk"></div>
		<!--footer-->
		<div class="footer">
			<div class="top">
				<div class="wrapper">
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot1.png"/></a>
						<span class="fl">7天无理由退货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot2.png"/></a>
						<span class="fl">15天免费换货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot3.png"/></a>
						<span class="fl">满599包邮</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot4.png"/></a>
						<span class="fl">手机特色服务</span>
					</div>
				</div>
			</div>
			<p class="dibu">最家家居&copy;2020公司版权所有 京ICP备XXX备XXX号<br />
			违法和不良信息举报电话：188-0130-1238，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</p>
		</div>
		<script src="../js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/jquery.SuperSlide.2.1.1.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/public.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/nav.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/pro.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/cart.js" type="text/javascript" charset="GBK"></script>
		<script type="text/javascript">
			jQuery(".bottom").slide({titCell:".hd ul",mainCell:".bd .likeList",autoPage:true,autoPlay:false,effect:"leftLoop",autoPlay:true,vis:1});
		</script>
	<script>
		//创建订单
		function addOrder(){
			let configurationText = null;
			for (let i = 0; i < $(".smallImg>p").length; i++) {
				if ($(".smallImg>p").eq(i).hasClass('on')){
					configurationText = i; //获取选择的商品配置索引
				}
			}
			if(configurationText==null){
				return;
			}
				axios({
					url:'http://localhost:9090/z/isLogin/GoodsController',
					params:{
						doing:"addOrder",
						id:${goods.id},
						noting:"true",
						configurationText:configurationText
					}
				}).then(function(result){ //function(result) 可以用result=>{}代替
					window.location.href = 'http://localhost:9090/z/isLogin/order.jsp?doing=sx'
				})
		}

		function addGoodsCar(noting){

			let configurationText = null;
			for (let i = 0; i < $(".smallImg>p").length; i++) {
				if ($(".smallImg>p").eq(i).hasClass('on')){
					configurationText = i; //获取选择的商品配置索引
				}
			}
			if(configurationText==null){
				return;
			}
			let count = document.getElementById('count'); //获取数量
			axios({
				url:'http://localhost:9090/z/isLogin/GoodsController',
				params:{
					doing:"addGoodsCar",
					goodsId:'${goods.id}',
					configurationText:configurationText,
					count:count.innerText
				}
			}).then(function(result){ //function(result) 可以用result=>{}代替
				if (noting==1){
					addOrder();
				}
				if (result.data==="ture"){
					// alert("添加成功")
					addCount()
				}
			})
			function addCount() {
				if ($(".categ p").hasClass("on")) {
					var num = parseInt($(".num span").text());
					var num1 = parseInt($(".goCart span").text());
					$(".goCart span").text(num + num1);
				}
			}

		}
	</script>
	</body>
</html>
