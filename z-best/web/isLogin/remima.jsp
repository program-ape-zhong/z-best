<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %> <!-- 1、编码 -->
<!DOCTYPE html>
<html>
	<head lang="en">
		<meta charset="utf-8" />
		<title>个人信息</title>
		<link rel="stylesheet" type="text/css" href="../css/public.css"/>
		<link rel="stylesheet" type="text/css" href="../css/mygrxx.css" />
		<script type="text/javascript" src="../js/hint.js"></script> <!-- 2、jsHint -->
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  <!-- 3、JSTL -->
	</head>
	<body>
	<div id="hint" style="z-index: 999"> <!-- 4、divHint -->
		${hint==""?"您已进入修改页面":hint}
		${sessionScope.remove("hint")}
	</div>
	<jsp:include page="../ggJsp/minHead.jsp"/> <!-- 5、minHead -->
		<!------------------------------idea------------------------------>
		<div class="address mt">
			<div class="wrapper clearfix">
				<a href="index.html" class="fl">首页</a>
				<span>/</span>
				<a href="mygxin.html" class="on">个人信息</a>
			</div>
		</div>
		
		<!------------------------------Bott------------------------------>
		<div class="Bott">
			<div class="wrapper clearfix">
				<jsp:include page="../ggJsp/bottLeft.jsp"/> <!-- 6、bottLeft -->
				<div class="you fl">
					<h2>修改密码</h2>
					<form action="${accountPath}?doing=updatePassword" method="post" class="remima">
						<p><span>选择验证身份方式：</span><input id="password" type="checkbox" checked onclick="alert('目前仅支持密码验证');document.getElementById('password').checked=true; "/>密码验证
							<input type="checkbox" id="email" onclick="alert('暂不支持邮箱验证');document.getElementById('email').checked=false;"  />邮箱验证 </p>
						<p><span>原密码：</span><input type="text" required name="oldPassword" /></p>
						<p class="op">输入原密码</p>
						<p><span>新密码：</span><input type="text" required name="newPassword" /></p>
						<p class="op">6-16 个字符，需使用字母、数字或符号组合，不能使用纯数字、纯字母、纯符号</p>
						<p><span>重复新密码：</span><input type="text" required name="newPassword2" /></p>
						<p class="op">请再次输入密码</p>
						<p><span>验证码：</span><input type="text" required name="code" /><img id="img" src="${accountPath}?doing=getCode" alt="" width="97" height="37" onclick="changeImg()"/></p>
						<p class="op">按右图输入验证码，不区分大小写</p>
						<input type="submit" value="提交" />
					</form>
				</div>
			</div>
		</div>
		
		<!--返回顶部-->
		<div class="gotop">
			<a href="cart.jsp">
			<dl>
				<dt><img src="../img/gt1.png"/></dt>
				<dd>去购<br />物车</dd>
			</dl>
			</a>
			<a href="#" class="dh">
			<dl>
				<dt><img src="../img/gt2.png"/></dt>
				<dd>联系<br />客服</dd>
			</dl>
			</a>
			<a href="mygxin.html">
			<dl>
				<dt><img src="../img/gt3.png"/></dt>
				<dd>个人<br />中心</dd>
			</dl>
			</a>
			<a href="#" class="toptop" style="display: none">
			<dl>
				<dt><img src="../img/gt4.png"/></dt>
				<dd>返回<br />顶部</dd>
			</dl>
			</a>
			<p>XXX</p>
		</div>
		<!--footer-->
		<div class="footer">
			<div class="top">
				<div class="wrapper">
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot1.png"/></a>
						<span class="fl">7天无理由退货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot2.png"/></a>
						<span class="fl">15天免费换货</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot3.png"/></a>
						<span class="fl">满599包邮</span>
					</div>
					<div class="clearfix">
						<a href="#2" class="fl"><img src="../img/foot4.png"/></a>
						<span class="fl">手机特色服务</span>
					</div>
				</div>
			</div>
			<p class="dibu">最家家居&copy;2020公司版权所有 京ICP备XXX备XXX号<br />
			违法和不良信息举报电话：XXX，本网站所列数据，除特殊说明，所有数据均出自我司实验室测试</p>
		</div>
		<script src="../js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/public.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/user.js" type="text/javascript" charset="utf-8"></script>
		<script>
			function changeImg() {
				document.getElementById("img").src = "${pageContext.request.contextPath}/isLogin/AccountController?doing=getCode&r=" + Math.random();
			}
		</script>
	</body>
</html>
