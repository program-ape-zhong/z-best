document.addEventListener("DOMContentLoaded", function () {
    // 在这里执行您的 JavaScript 代码
    var hint = document.getElementById('hint');
    if (hint.textContent == "") {
        hint.style.opacity = "0";
    } else {
        //元素显示
        // setTimeout(xx,100);
        xx();
        hint.style.opacity = "1";
        //三秒后元素隐藏
        setTimeout(yc, 3000);
    }

//隐藏
    function yc() {
        var i = 1;

        //递归函数
        function fadeOut() {
            hint.style.opacity = i;
            i -= 0.1;

            if (i >= 0) {
                setTimeout(fadeOut, 30);
            }
            console.log(i);
        }

        fadeOut(); // 开始逐渐透明
    }

//显现
    function xx() {
        var i = 0;

        //递归函数
        function fadeOut() {
            hint.style.opacity = i;
            i += 0.1;

            if (i <= 1) {
                setTimeout(fadeOut, 30);
            }
            console.log(i);
        }

        fadeOut(); // 开始逐渐透明
    }
});
