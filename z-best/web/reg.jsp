<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>注册</title>
		<link rel="stylesheet" type="text/css" href="css/public.css"/>
		<link rel="stylesheet" type="text/css" href="css/login.css"/>
		<script type="text/javascript" src="js/hint.js"></script>
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	</head>
	<body>
	<div id="hint" style="z-index: 1">
		${hint==""?"您已进入注册页面":hint}
		${sessionScope.remove("hint")}
	</div>
		<!-------------------reg-------------------------->
		<div class="reg">
			<form action="${pageContext.request.contextPath}/isLogin/AccountController?doing=register" method="post">
				<h1><a href="isLogin/index.jsp"><img src="img/temp/logo.png"></a></h1>
				<p>用户注册</p>
				<p><input type="number" name="phone" required value="" placeholder="请输入手机号"></p>
				<p><input type="text" name="password1" required value="" placeholder="请输入密码"></p>
				<p><input type="text" name="password2" required value="" placeholder="请确认密码"></p>
				<p class="txtL txt"><input class="code" required type="text" name="code" value="" placeholder="验证码"><img src="${pageContext.request.contextPath}/isLogin/AccountController?doing=getCode" class="code" id="img" alt="网络异常" onclick = changeImg()></p>
				<p><input type="submit" name="" value="注册"></p>
				<p class="txtL txt">完成此注册，即表明您同意了我们的<a href="#">使用条款和隐私策略</a></p>
				<p class="txt"><a href="login.jsp"><span></span>已有账号登录</a></p>
			</form>
		</div>
		<script>
			function changeImg() {
				document.getElementById("img").src = "${pageContext.request.contextPath}/isLogin/AccountController?doing=getCode&r=" + Math.random();
			}
		</script>
	</body>
</html>
